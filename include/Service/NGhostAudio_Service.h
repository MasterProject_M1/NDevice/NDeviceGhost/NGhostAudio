#ifndef NGhostAudio_Service_PROTECT
#define NGhostAudio_Service_PROTECT

// ------------------------------
// namespace NGhostAudio::Service
// ------------------------------

// enum NGhostAudio::Service::NGhostAudioServiceType
#include "NGhostAudio_Service_NGhostAudioServiceType.h"

// namespace NGhostAudio::Service::Audio
#include "Audio/NGhostAudio_Service_Audio.h"

/**
 * Process an HTTP request
 *
 * @param request
 * 		The http request
 * @param client
 * 		The requesting client
 *
 * @return the HTTP response
 */
__ALLOC NReponseHTTP *NGhostAudio_Service_ProcessRequest( const NRequeteHTTP *request,
	const NClientServeur *client );

#endif // !NGhostAudio_Service_PROTECT
