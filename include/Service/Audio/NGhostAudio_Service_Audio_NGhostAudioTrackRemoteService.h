#ifndef NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKREMOTESERVICE_PROTECT
#define NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKREMOTESERVICE_PROTECT

// ---------------------------------------------------------------
// enum NGhostAudio::Service::Audio::NGhostAudioTrackRemoteService
// ---------------------------------------------------------------

typedef enum NGhostAudioTrackRemoteService
{
	NGHOSTAUDIO_TRACK_REMOTE_SERVICE_ROOT,

	NGHOSTAUDIO_TRACK_REMOTE_SERVICE_HOSTNAME,
	NGHOSTAUDIO_TRACK_REMOTE_SERVICE_PORT,
	NGHOSTAUDIO_TRACK_REMOTE_SERVICE_FILE_PATH,

	NGHOSTAUDIO_TRACK_REMOTE_SERVICES
} NGhostAudioTrackRemoteService;

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostAudioTrackRemoteService NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetName( NGhostAudioTrackRemoteService serviceType );

/**
 * Get service full path
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service full path
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGhostAudioTrackRemoteService serviceType );

#ifdef NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKREMOTESERVICE_INTERNE
__PRIVATE const char NGhostAudioTrackRemoteServiceName[ NGHOSTAUDIO_TRACK_REMOTE_SERVICES ][ 32 ] =
{
	"",

	"hostname",
	"port",
	"filePath"
};

__PRIVATE const char NGhostAudioTrackRemoteServiceFullPath[ NGHOSTAUDIO_TRACK_REMOTE_SERVICES ][ 32 ] =
{
	"",

	NGHOSTAUDIO_SERVICE_AUDIO_REMOTE_ROOT".hostname",
	NGHOSTAUDIO_SERVICE_AUDIO_REMOTE_ROOT".port",
	NGHOSTAUDIO_SERVICE_AUDIO_REMOTE_ROOT".filePath"
};
#endif // NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKREMOTESERVICE_INTERNE

#endif // !NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKREMOTESERVICE_PROTECT
