#ifndef NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSTATESERVICE_PROTECT
#define NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSTATESERVICE_PROTECT

// --------------------------------------------------------------
// enum NGhostAudio::Service::Audio::NGhostAudioTrackStateService
// --------------------------------------------------------------

typedef enum NGhostAudioTrackStateService
{
	NGHOST_AUDIO_TRACK_STATE_SERVICE_ROOT,

	NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_READY,
	NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED,
	NGHOST_AUDIO_TRACK_STATE_SERVICE_VOLUME,
	NGHOST_AUDIO_TRACK_STATE_SERVICE_POSITION,

	NGHOST_AUDIO_TRACK_STATE_SERVICES
} NGhostAudioTrackStateService;

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostAudioTrackStateService NGhostAudio_Service_Audio_NGhostAudioTrackStateService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetName( NGhostAudioTrackStateService serviceType );

/**
 * Get full path
 *
 * @param serviceType
 * 		The service type
 *
 * @return the full path
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGhostAudioTrackStateService serviceType );

#ifdef NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSTATESERVICE_INTERNE
__PRIVATE const char NGhostAudioTrackStateServiceName[ NGHOST_AUDIO_TRACK_STATE_SERVICES ][ 32 ] =
{
	"",
	
	"isReady",
	"isPaused",
	"volume",
	"position"
};

__PRIVATE const char NGhostAudioTrackStateServiceFullPath[ NGHOST_AUDIO_TRACK_STATE_SERVICES ][ 32 ] =
{
	"",

	NGHOSTAUDIO_SERVICE_AUDIO_STATE_ROOT".isReady",
	NGHOSTAUDIO_SERVICE_AUDIO_STATE_ROOT".isPaused",
	NGHOSTAUDIO_SERVICE_AUDIO_STATE_ROOT".volume",
	NGHOSTAUDIO_SERVICE_AUDIO_STATE_ROOT".position"
};
#endif // NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSTATESERVICE_INTERNE

#endif // !NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSTATESERVICE_PROTECT
