#ifndef NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOPLAYERSERVICE_PROTECT
#define NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOPLAYERSERVICE_PROTECT

// ----------------------------------------------------------
// enum NGhostAudio::Service::Audio::NGhostAudioPlayerService
// ----------------------------------------------------------

typedef enum NGhostAudioPlayerService
{
	NGHOST_AUDIO_PLAYER_SERVICE_ROOT,

	NGHOST_AUDIO_PLAYER_SERVICE_TRACK_COUNT,
	NGHOST_AUDIO_PLAYER_SERVICE_TRACK,

	NGHOST_AUDIO_PLAYER_SERVICES
} NGhostAudioPlayerService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param ghostAudioPlayer
 * 		The protected ghost audio player
 * @param trackIndex
 * 		The requested track index
 *
 * @return the service or NGHOST_AUDIO_PLAYER_SERVICES
 */
NGhostAudioPlayerService NGhostAudio_Service_Audio_NGhostAudioPlayerService_FindService( const char *requestedElement,
	NU32 *cursor,
	__MUSTBEPROTECTED const NGhostAudioPlayer *ghostAudioPlayer,
	__OUTPUT NU32 *trackIndex );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostAudio_Service_Audio_NGhostAudioPlayerService_GetName( NGhostAudioPlayerService serviceType );

#ifdef NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOPLAYERSERVICE_INTERNE
__PRIVATE const char NGhostAudioPlayerServiceName[ NGHOST_AUDIO_PLAYER_SERVICES ][ 32 ] =
{
	"",

	"trackCount",
	"track"
};
#endif // NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOPLAYERSERVICE_INTERNE

#endif // !NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOPLAYERSERVICE_PROTECT
