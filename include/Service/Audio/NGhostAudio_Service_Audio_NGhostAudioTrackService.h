#ifndef NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSERVICE_PROTECT
#define NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSERVICE_PROTECT

// ---------------------------------------------------------
// enum NGhostAudio::Service::Audio::NGhostAudioTrackService
// ---------------------------------------------------------

typedef enum NGhostAudioTrackService
{
	NGHOST_AUDIO_TRACK_SERVICE_ROOT,

	NGHOST_AUDIO_TRACK_SERVICE_NAME,
	NGHOST_AUDIO_TRACK_SERVICE_IS_LOOP,
	NGHOST_AUDIO_TRACK_SERVICE_LENGTH,

	NGHOST_AUDIO_TRACK_SERVICE_SYNCHRONIZATION_LIST,

	NGHOST_AUDIO_TRACK_SERVICE_REMOTE,
	NGHOST_AUDIO_TRACK_SERVICE_STATE,

	NGHOST_AUDIO_TRACK_SERVICES
} NGhostAudioTrackService;

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostAudioTrackService NGhostAudio_Service_Audio_NGhostAudioTrackService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackService_GetName( NGhostAudioTrackService serviceType );

#ifdef NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSERVICE_INTERNE
__PRIVATE const char NGhostAudioTrackServiceName[ NGHOST_AUDIO_TRACK_SERVICES ][ 32 ] =
{
	"",

	"name",
	"isLoop",
	"length",

	"synchList",

	NGHOSTAUDIO_SERVICE_AUDIO_REMOTE_ROOT,
	NGHOSTAUDIO_SERVICE_AUDIO_STATE_ROOT
};
#endif // NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSERVICE_INTERNE

#endif // !NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSERVICE_PROTECT
