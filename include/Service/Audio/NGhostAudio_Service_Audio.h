#ifndef NGHOSTAUDIO_SERVICE_AUDIO_PROTECT
#define NGHOSTAUDIO_SERVICE_AUDIO_PROTECT

// -------------------------------------
// namespace NGhostAudio::Service::Audio
// -------------------------------------

// Subpaths
#define NGHOSTAUDIO_SERVICE_AUDIO_REMOTE_ROOT		"remote"
#define NGHOSTAUDIO_SERVICE_AUDIO_STATE_ROOT		"state"

// enum NGhostAudio::Service::Audio::NGhostAudioTrackService
#include "NGhostAudio_Service_Audio_NGhostAudioTrackService.h"

// enum NGhostAudio::Service::Audio::NGhostAudioTrackRemoteService
#include "NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService.h"

// enum NGhostAudio::Service::Audio::NGhostAudioTrackStateService
#include "NGhostAudio_Service_Audio_NGhostAudioTrackStateService.h"

// enum NGhostAudio::Service::Audio::NGhostAudioPlayerService
#include "NGhostAudio_Service_Audio_NGhostAudioPlayerService.h"

/**
 * Process REST request
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param requestType
 * 		The http request type
 * @param data
 * 		The user sent data
 * @param dataLength
 * 		The user sent data length
 * @param ghostAudio
 * 		The ghost audio instance
 * @param clientID
 * 		The client id
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostAudio_Service_Audio_ProcessRESTRequest( const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP requestType,
	const char *data,
	NU32 dataLength,
	void *ghostAudio,
	NU32 clientID );

#endif // !NGHOSTAUDIO_SERVICE_AUDIO_PROTECT
