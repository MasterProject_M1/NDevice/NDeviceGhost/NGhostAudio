#ifndef NGHOSTAUDIO_SERVICE_NGHOSTAUDIOSERVICETYPE_PROTECT
#define NGHOSTAUDIO_SERVICE_NGHOSTAUDIOSERVICETYPE_PROTECT

// -------------------------------------------------
// enum NGhostAudio::Service::NGhostAudioServiceType
// -------------------------------------------------

typedef enum NGhostAudioServiceType
{
	NGHOST_AUDIO_SERVICE_TYPE_AUDIO_GET,
	NGHOST_AUDIO_SERVICE_TYPE_AUDIO_POST,
	NGHOST_AUDIO_SERVICE_TYPE_AUDIO_PUT,
	NGHOST_AUDIO_SERVICE_TYPE_AUDIO_DELETE,

	NGHOST_AUDIO_SERVICE_TYPES
} NGhostAudioServiceType;

/**
 * Get name API
 *
 * @param serviceType
 * 		The service type
 *
 * @return the api URI
 */
const char *NGhostAudio_Service_NGhostAudioServiceType_GetServiceURI( NGhostAudioServiceType serviceType );

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param httpRequestType
 * 		The http request type
 *
 * @return the service type
 */
NGhostAudioServiceType NGhostAudio_Service_NGhostAudioServiceType_FindService( const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP httpRequestType );

#ifdef NGHOSTAUDIO_SERVICE_NGHOSTAUDIOSERVICETYPE_INTERNE
static const char NGhostAudioServiceTypeURI[ NGHOST_AUDIO_SERVICE_TYPES ][ 64 ] =
{
	NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE,
	NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE,
	NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE,
	NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE
};

static const NTypeRequeteHTTP NGhostAudioServiceHTTPRequestType[ NGHOST_AUDIO_SERVICE_TYPES ] =
{
	NTYPE_REQUETE_HTTP_GET,
	NTYPE_REQUETE_HTTP_POST,
	NTYPE_REQUETE_HTTP_PUT,
	NTYPE_REQUETE_HTTP_DELETE
};

#endif // NGHOSTAUDIO_SERVICE_NGHOSTAUDIOSERVICETYPE_INTERNE

#endif // !NGHOSTAUDIO_SERVICE_NGHOSTAUDIOSERVICETYPE_PROTECT
