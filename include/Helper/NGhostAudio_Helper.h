#ifndef NGHOSTAUDIO_HELPER_PROTECT
#define NGHOSTAUDIO_HELPER_PROTECT

// -----------------------------
// namespace NGhostAudio::Helper
// -----------------------------

/**
 * Build version
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Helper_BuildVersion( __OUTPUT char version[ 32 ] );

#endif // !NGHOSTAUDIO_HELPER_PROTECT
