#ifndef NGHOSTAUDIO_ACTION_PROTECT
#define NGHOSTAUDIO_ACTION_PROTECT

// -----------------------------
// namespace NGhostAudio::Action
// -----------------------------

#define NGHOST_AUDIO_DELAY_BETWEEN_UPDATE				15000

/**
 * Update actions
 *
 * @param actionUpdater
 * 		The action updater
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Action_UpdateThread( NGhostActionUpdater *actionUpdater );

#endif // !NGHOSTAUDIO_ACTION_PROTECT
