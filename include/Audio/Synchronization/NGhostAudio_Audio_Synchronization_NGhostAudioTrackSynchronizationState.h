#ifndef NGHOSTAUDIO_AUDIO_SYNCHRONIZATION_NGHOSTAUDIOTRACKSYNCHRONIZATIONSTATE_PROTECT
#define NGHOSTAUDIO_AUDIO_SYNCHRONIZATION_NGHOSTAUDIOTRACKSYNCHRONIZATIONSTATE_PROTECT

// --------------------------------------------------------------------------------
// struct NGhostAudio::Audio::Synchronization::NGhostAudioTrackSynchronizationState
// --------------------------------------------------------------------------------

typedef struct NGhostAudioTrackSynchronizationState
{
	// HTTP client
	NClientHTTP *m_client;

	// Ghost audio object
	void *m_ghostAudioObject;

	// Audio source authentication entry
	const NAuthenticationEntry *m_audioSourceAuthenticationEntry;

	// Remote player authentication entry
	const NAuthenticationEntry *m_audioPlayerAuthenticationEntry;

	// Track name (id)
	char *m_trackName;

	// Is ready?
	NBOOL m_isReady;

	// Is waiting for track add?
	NBOOL m_isWaitingTrackAdd;

	// Synchronization thread
	NThread *m_synchronizationThread;

	// Is synchronization thread running?
	NBOOL m_isSynchronizationThreadRunning;
} NGhostAudioTrackSynchronizationState;

/**
 * Build remote audio state
 *
 * @param hostname
 * 		The remote hostname to synchronize with
 * @param ghostAudioObject
 * 		The ghost audio object
 * @param authenticationManager
 * 		The authentication manager
 * @param audioSourceAuthenticationEntry
 * 		The audio source authentication entry
 *
 * @return the built instance
 */
__ALLOC NGhostAudioTrackSynchronizationState *NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_Build( const char *hostname,
	void *ghostAudioObject,
	const NAuthenticationManager *authenticationManager );

/**
 * Destroy instance
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_Destroy( NGhostAudioTrackSynchronizationState** );

/**
 * Is ready?
 *
 * @param this
 * 		This instance
 *
 * @return if the remote client is ready
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_IsReady( const NGhostAudioTrackSynchronizationState* );

/**
 * Set is paused
 *
 * @param this
 * 		This instance
 * @param isPaused
 * 		Paused?
 * @param position
 * 		The position to set at startup (NERREUR to ignore)
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetPause( NGhostAudioTrackSynchronizationState *this,
	NBOOL isPaused,
	NU32 position );

/**
 * Set volume
 *
 * @param this
 * 		This instance
 * @param volume
 * 		The volume (0-100)
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetVolume( NGhostAudioTrackSynchronizationState*,
	NU32 volume );

/**
 * Set position
 *
 * @param this
 * 		This instance
 * @param position
 * 		The position
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetPosition( NGhostAudioTrackSynchronizationState*,
	NU32 position );

/**
 * Is error
 *
 * @param this
 * 		This instance
 *
 * @return if there has been an error
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_IsError( const NGhostAudioTrackSynchronizationState* );

#endif // !NGHOSTAUDIO_AUDIO_SYNCHRONIZATION_NGHOSTAUDIOTRACKSYNCHRONIZATIONSTATE_PROTECT
