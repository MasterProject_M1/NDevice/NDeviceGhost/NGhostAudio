#ifndef NGHOSTAUDIO_AUDIO_SYNCHRONIZATION_NGHOSTAUDIOSYNCHRONIZER_PROTECT
#define NGHOSTAUDIO_AUDIO_SYNCHRONIZATION_NGHOSTAUDIOSYNCHRONIZER_PROTECT

// -------------------------------------------------------------------
// struct NGhostAudio::Audio::Synchronization::NGhostAudioSynchronizer
// -------------------------------------------------------------------

typedef struct NGhostAudioSynchronizer
{
	// Remote location list (NListe<NGhostAudioTrackSynchronizationState*>)
	NListe *m_remote;

	// Ghost audio object
	void *m_ghostAudioObject;
} NGhostAudioSynchronizer;

/**
 * Build synchronizer
 *
 * @param synchList
 * 		The synchronization list
 * @param ghostAudioObject
 * 		The audio instance
 * @param authenticationManager
 * 		The authentication manager
 *
 * @return the synchronizer instance
 */
__ALLOC NGhostAudioSynchronizer *NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_Build( const NListe *synchList,
	void *ghostAudioObject,
	const NAuthenticationManager *authenticationManager );

/**
 * Destroy synchronizer
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_Destroy( NGhostAudioSynchronizer** );

/**
 * Is synchronized?
 *
 * @param this
 * 		This instance
 *
 * @return if all the remote players are ready for simultaneous command
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_IsSynchronized( const NGhostAudioSynchronizer* );

/**
 * Synchronized start
 *
 * @param this
 * 		This instance
 * @param position
 * 		The position in the audio (ms, NERREUR to ignore)
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizeStart( NGhostAudioSynchronizer *this,
	NU32 position );

/**
 * Synchronized stop
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizePause( NGhostAudioSynchronizer * );

/**
 * Synchronized volume set
 *
 * @param this
 * 		This instance
 * @param volume
 * 		The volume to be set
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizeVolumeSet( NGhostAudioSynchronizer*,
	NU32 volume );

/**
 * Synchronized position set
 *
 * @param this
 * 		This instance
 * @param position
 * 		The position to be set
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizePositionSet( NGhostAudioSynchronizer*,
	NU32 position );

#endif // !NGHOSTAUDIO_AUDIO_SYNCHRONIZATION_NGHOSTAUDIOSYNCHRONIZER_PROTECT
