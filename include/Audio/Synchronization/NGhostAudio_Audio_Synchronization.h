#ifndef NGHOSTAUDIO_AUDIO_SYNCHRONIZATION_PROTECT
#define NGHOSTAUDIO_AUDIO_SYNCHRONIZATION_PROTECT

// ---------------------------------------------
// namespace NGhostAudio::Audio::Synchronization
// ---------------------------------------------

// Synchronization timeout
#define NGHOST_AUDIO_SYNCHRONIZATION_CONNECTION_TIMEOUT					5000

// Delay between synchronization attempt
#define NGHOST_AUDIO_SYNCHRONIZATION_RETRY_DELAY						4000

// Delay before local track start
#define NGHOST_AUDIO_SYNCHRONIZATION_DELAY_BEFORE_START					200

// Maximum retry count for initial POST request
#define NGHOST_AUDIO_SYNCHRONIZATION_MAXIMUM_INITIAL_POST_REQUEST		20

// struct NGhostAudio::Audio::Synchronization::NGhostAudioTrackSynchronizationState
#include "NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState.h"

// struct NGhostAudio::Audio::Synchronization::NGhostAudioSynchronizer
#include "NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer.h"

#endif // !NGHOSTAUDIO_AUDIO_SYNCHRONIZATION_PROTECT
