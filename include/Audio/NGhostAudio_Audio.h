#ifndef NGHOSTAUDIO_AUDIO_PROTECT
#define NGHOSTAUDIO_AUDIO_PROTECT

// ----------------------------
// namespace NGhostAudio::Audio
// ----------------------------

// Connection timeout
#define NGHOSTAUDIO_AUDIO_CONNECTION_TIMEOUT			1000

// Music directory
#define NGHOSTAUDIO_AUDIO_MUSIC_DIRECTORY				"Music"

// Delay
#define NGHOSTAUDIO_AUDIO_DELAY_BETWEEN_CLEANING_STEP	100

// struct NGhostAudio::Audio::NGhostAudioTrackInfo
#include "NGhostAudio_Audio_NGhostAudioTrackInfo.h"

// namespace NGhostAudio::Audio::Synchronization
#include "Synchronization/NGhostAudio_Audio_Synchronization.h"

// struct NGhostAudio::Audio::NGhostAudioRemoteAudio
#include "NGhostAudio_Audio_NGhostAudioRemoteAudio.h"

// struct NGhostAudio::Audio::NGhostAudioObject
#include "NGhostAudio_Audio_NGhostAudioObject.h"

// struct NGhostAudio::Audio::NGhostAudioPlayer
#include "NGhostAudio_Audio_NGhostAudioPlayer.h"

#endif // !NGHOSTAUDIO_AUDIO_PROTECT
