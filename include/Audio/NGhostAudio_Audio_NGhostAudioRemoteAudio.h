#ifndef NGHOSTAUDIO_AUDIO_NGHOSTAUDIOREMOTEAUDIO_PROTECT
#define NGHOSTAUDIO_AUDIO_NGHOSTAUDIOREMOTEAUDIO_PROTECT

// -------------------------------------------------
// struct NGhostAudio::Audio::NGhostAudioRemoteAudio
// -------------------------------------------------

typedef struct NGhostAudioRemoteAudio
{
	// Remote authentication entry
	const NAuthenticationEntry *m_authenticationEntry;

	// Remote file path
	char *m_remoteFilePath;

	// Track name
	char *m_trackName;

	// Track download thread
	NThread *m_downloadThread;

	// Is error?
	NBOOL m_isError;

	// Is loading done?
	NBOOL m_isLoadingDone;

	// Audio object callback
	void *m_ghostAudioObject;

	// Synchronizer
	NGhostAudioSynchronizer *m_synchronizer;
} NGhostAudioRemoteAudio;

/**
 * Build remote audio instance
 *
 * @param hostname
 * 		The remote hostname
 * @param port
 * 		The remote port
 * @param authenticationManager
 * 		The authentication manager
 * @param filePath
 * 		The remote file path
 * @param trackName
 * 		The track name
 * @param ghostAudioObject
 * 		The ghost audio object
 *
 * @return the instance
 */
__ALLOC NGhostAudioRemoteAudio *NGhostAudio_Audio_NGhostAudioRemoteAudio_Build( const char *hostname,
	NU32 port,
	const NAuthenticationManager *authenticationManager,
	const char *filePath,
	const char *trackName,
	void *ghostAudioObject );

/**
 * Build synchronizer
 *
 * @param this
 * 		This instance
 * @param authenticationManager
 * 		The authentication manager
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildSynchronizer( NGhostAudioRemoteAudio*,
	const NAuthenticationManager *authenticationManager );

/**
 * Destroy remote audio instance
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioRemoteAudio_Destroy( NGhostAudioRemoteAudio** );

/**
 * Get authentication entry
 *
 * @param this
 * 		This instance
 *
 * @return the authentication entry
 */
const NAuthenticationEntry *NGhostAudio_Audio_NGhostAudioRemoteAudio_GetAuthenticationEntry( const NGhostAudioRemoteAudio* );

/**
 * Get remote file path
 *
 * @param this
 * 		This instance
 *
 * @return the remote file path
 */
const char *NGhostAudio_Audio_NGhostAudioRemoteAudio_GetFilePath( const NGhostAudioRemoteAudio* );

/**
 * Get track name
 *
 * @param this
 * 		This instance
 *
 * @return the track name
 */
const char *NGhostAudio_Audio_NGhostAudioRemoteAudio_GetTrackName( const NGhostAudioRemoteAudio* );

/**
 * Is ready?
 *
 * @param this
 * 		This instance
 *
 * @return if ready
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_IsReady( const NGhostAudioRemoteAudio* );

/**
 * Is synchronized?
 *
 * @param this
 * 		This instance
 *
 * @return if synchronized with other players
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_IsSynchronized( const NGhostAudioRemoteAudio* );

/**
 * Is loading done?
 *
 * @param this
 * 		This instance
 *
 * @return if the loading is done
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_IsLoadingDone( const NGhostAudioRemoteAudio* );

/**
 * Is there an error?
 *
 * @param this
 * 		This instance
 *
 * @return if there is an error
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_IsError( const NGhostAudioRemoteAudio* );

/**
 * Activate error flag
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioRemoteAudio_SetIsError( NGhostAudioRemoteAudio* );

/**
 * Synchronized start
 *
 * @param this
 * 		This instance
 * @param position
 * 		The position at startup (ms, NERREUR to ignore)
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizeStart( NGhostAudioRemoteAudio*,
	NU32 position );

/**
 * Synchronized pause
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizePause( NGhostAudioRemoteAudio* );

/**
 * Synchronized volume change
 *
 * @param this
 * 		This instance
 * @param volume
 * 		The new volume to be set
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizeVolumeSet( NGhostAudioRemoteAudio*,
	NU32 volume );

/**
 * Synchronized position change
 *
 * @param this
 * 		This instance
 * @param position
 * 		The new position to be set
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizePositionSet( NGhostAudioRemoteAudio*,
	NU32 position );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param serviceType
 * 		The service type (NGhostAudioTrackRemoteService)
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildParserOutputListInternal( const NGhostAudioRemoteAudio*,
	NU32 serviceType,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *root );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildParserOutputList( const NGhostAudioRemoteAudio*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *root );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_ProcessRESTGETRequest( const NGhostAudioRemoteAudio*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor );

#endif // !NGHOSTAUDIO_AUDIO_NGHOSTAUDIOREMOTEAUDIO_PROTECT
