#ifndef NGHOSTAUDIO_AUDIO_NGHOSTAUDIOOBJECT_PROTECT
#define NGHOSTAUDIO_AUDIO_NGHOSTAUDIOOBJECT_PROTECT

// --------------------------------------------
// struct NGhostAudio::Audio::NGhostAudioObject
// --------------------------------------------

typedef struct NGhostAudioObject
{
	// Remote object details
	NGhostAudioRemoteAudio *m_remote;

	// Audio instance
	NMusique *m_audio;

	// Last update time
	NU64 m_lastUpdateTime;

	// Volume
	NU32 m_volume;

	// Is must start as soon as possible?
	NBOOL m_isMustStart;

	// Position to set after loading
	NU32 m_initialPosition;

	// Is sound looping?
	NBOOL m_isLoop;

	// Synchronized list (char*)
	NListe *m_synchronizedList;
} NGhostAudioObject;

/**
 * Build ghost audio object instance
 *
 * @param trackInfo
 * 		The track informations
 * @param authenticationManager
 * 		The authentication manager
 *
 * @return the audio object instance
 */
__ALLOC NGhostAudioObject *NGhostAudio_Audio_NGhostAudioObject_Build( __WILLBEOWNED NGhostAudioTrackInfo *trackInfo,
	const NAuthenticationManager *authenticationManager );

/**
 * Destroy audio instance
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioObject_Destroy( NGhostAudioObject** );

/**
 * Get name
 *
 * @param this
 * 		This instance
 *
 * @return the name
 */
const char *NGhostAudio_Audio_NGhostAudioObject_GetName( const NGhostAudioObject* );

/**
 * Is paused?
 *
 * @param this
 * 		This instance
 *
 * @return if the audio is paused
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_IsPaused( const NGhostAudioObject* );

/**
 * Is there an error?
 *
 * @param this
 * 		This instance
 *
 * @return if there is an error
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_IsError( const NGhostAudioObject* );

/**
 * Is loading done?
 *
 * @param this
 * 		This instance
 *
 * @return if the loading is done
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_IsLoadingDone( const NGhostAudioObject* );

/**
 * Is loop ?
 *
 * @param this
 * 		This instance
 *
 * @return if loop
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_IsLoop( const NGhostAudioObject* );

/**
 * Get current position (ms) in audio
 *
 * @param this
 * 		This instance
 *
 * @return the audio position (ms)
 */
NU32 NGhostAudio_Audio_NGhostAudioObject_GetPosition( const NGhostAudioObject* );

/**
 * Get volume
 *
 * @param this
 * 		This instance
 *
 * @return the volume (0-100)
 */
NU32 NGhostAudio_Audio_NGhostAudioObject_GetVolume( const NGhostAudioObject* );

/**
 * Get audio
 *
 * @param this
 * 		This instance
 *
 * @return the audio instance
 */
NMusique *NGhostAudio_Audio_NGhostAudioObject_GetAudio( const NGhostAudioObject* );

/**
 * Get remote file path
 *
 * @param this
 * 		This instance
 *
 * @return the remote file path
 */
const char *NGhostAudio_Audio_NGhostAudioObject_GetRemoteFilePath( const NGhostAudioObject* );

/**
 * Get synchronization list
 *
 * @param this
 * 		This instance
 *
 * @return the synchronization list
 */
const NListe *NGhostAudio_Audio_NGhostAudioObject_GetSynchronizationList( const NGhostAudioObject* );

/**
 * Get authentication entry
 *
 * @param this
 * 		This instance
 *
 * @return the authentication entry
 */
const NAuthenticationEntry *NGhostAudio_Audio_NGhostAudioObject_GetAuthenticationEntry( const NGhostAudioObject* );

/**
 * Request track suppression
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioObject_RequestSuppression( NGhostAudioObject* );

/**
 * Load audio
 *
 * @param this
 * 		This instance
 * @param filePath
 * 		The filepath
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_LoadAudio( NGhostAudioObject*,
	const char *filePath );

/**
 * Process operation after load completed
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_ProcessLoadCallback( NGhostAudioObject* );

/**
 * Build track parser output list internal
 *
 * @param this
 * 		This instance
 * @param serviceType
 * 		The service type (NGhostAudioTrackService)
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_BuildTrackParserOutputListInternal( const NGhostAudioObject*,
	NU32 serviceType,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *root );

/**
 * Build track parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_BuildTrackParserOutputList( const NGhostAudioObject *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *root );

/**
 * Process track GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_ProcessTrackGETRequest( const NGhostAudioObject*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor );

/**
 * Process track PUT request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_ProcessTrackPUTRequest( NGhostAudioObject *this,
	const char *requestedElement,
	NU32 *cursor,
	const NParserOutputList *userData );

#endif // !NGHOSTAUDIO_AUDIO_NGHOSTAUDIOOBJECT_PROTECT
