#ifndef NGHOSTAUDIO_AUDIO_NGHOSTAUDIOTRACKINFO_PROTECT
#define NGHOSTAUDIO_AUDIO_NGHOSTAUDIOTRACKINFO_PROTECT

// -----------------------------------------------
// struct NGhostAudio::Audio::NGhostAudioTrackInfo
// -----------------------------------------------

typedef struct NGhostAudioTrackInfo
{
	// Track name
	const char *m_trackName;

	// Hostname
	const char *m_hostname;

	// Port
	NU32 m_port;

	// File path
	const char *m_filePath;

	// Synchronized list
	NListe *m_synchList;

	// Is loop?
	NBOOL m_isLoop;

	// Is must start?
	NBOOL m_isMustStart;

	// Volume
	NU32 m_volume;

	// Position
	NU32 m_position;
} NGhostAudioTrackInfo;

/**
 * Destroy track info
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioTrackInfo_Clean( NGhostAudioTrackInfo *this );

/**
 * Extract user track info
 *
 * @param userData
 * 		The user data
 * @param trackInfo
 * 		The extracted track informations
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioPlayer_ExtractUserTrackInfo( const NParserOutputList *userData,
	__OUTPUT NGhostAudioTrackInfo *trackInfo );

#endif // !NGHOSTAUDIO_AUDIO_NGHOSTAUDIOTRACKINFO_PROTECT
