#ifndef NGHOSTAUDIO_AUDIO_NGHOSTAUDIOPLAYER_PROTECT
#define NGHOSTAUDIO_AUDIO_NGHOSTAUDIOPLAYER_PROTECT

// --------------------------------------------
// struct NGhostAudio::Audio::NGhostAudioPlayer
// --------------------------------------------

#include "NGhostAudio_Audio_NGhostAudioTrackInfo.h"

typedef struct NGhostAudioPlayer
{
	// Authentication manager
	NAuthenticationManager *m_authenticationManager;

	// Audio object list (NListe<NGhostAudioObject*>)
	NListe *m_object;

	// Is cleaning thread running?
	NBOOL m_isCleaningThreadRunning;

	// Cleaning thread
	NThread *m_cleaningThread;
} NGhostAudioPlayer;

/**
 * Build ghost audio player
 *
 * @param authenticationManager
 * 		The authentication manager
 *
 * @return the ghost audio player
 */
__ALLOC NGhostAudioPlayer *NGhostAudio_Audio_NGhostAudioPlayer_Build( NAuthenticationManager *authenticationManager );

/**
 * Destroy audio player
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioPlayer_Destroy( NGhostAudioPlayer** );

/**
 * Add a song
 *
 * @param this
 * 		This instance
 * @param trackInfo
 * 		The track informations
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__WILLLOCK __WILLUNLOCK __ALLOC NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_AddAudioObject( NGhostAudioPlayer*,
	const NGhostAudioTrackInfo *trackInfo,
	NU32 clientID );

/**
 * Find audio object index with name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The audio object name
 *
 * @return the audio object index or NERREUR
 */
__MUSTBEPROTECTED NU32 NGhostAudio_Audio_NGhostAudioPlayer_FindAudioObjectIndexWithName( const NGhostAudioPlayer*,
	const char *name );

/**
 * Find audio object with name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The audio object name
 *
 * @return the audio object instance or NULL
 */
__MUSTBEPROTECTED const NGhostAudioObject *NGhostAudio_Audio_NGhostAudioPlayer_FindAudioObjectWithName( const NGhostAudioPlayer*,
	const char *name );

/**
 * Get track count
 *
 * @param this
 * 		This instance
 *
 * @return the track count
 */
__MUSTBEPROTECTED NU32 NGhostAudio_Audio_NGhostAudioPlayer_GetTrackCount( const NGhostAudioPlayer* );

/**
 * Request track suppression by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The track index
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostAudio_Audio_NGhostAudioPlayer_RequestTrackSuppression( NGhostAudioPlayer*,
	NU32 index );

/**
 * Process POST request
 *
 * @param this
 * 		This instance
 * @param userData
 * 		The user data
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_ProcessRESTPostRequest( NGhostAudioPlayer*,
	NParserOutputList *userData,
	NU32 clientID );

/**
 * Process PUT request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The user data
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC __WILLLOCK __WILLUNLOCK NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_ProcessRESTPUTRequest( NGhostAudioPlayer*,
	const char *requestedElement,
	NU32 *cursor,
	const NParserOutputList *userData,
	NU32 clientID );

/**
 * Process GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC __WILLLOCK __WILLUNLOCK NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_ProcessGETRequest( NGhostAudioPlayer*,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID );

/**
 * Process DELETE request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		Cursor in requested element
 * @param clientID
 * 		The client ID
 *
 * @return the response
 */
__ALLOC NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_ProcessRESTDeleteRequest( NGhostAudioPlayer*,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID );

#endif // !NGHOSTAUDIO_AUDIO_NGHOSTAUDIOPLAYER_PROTECT
