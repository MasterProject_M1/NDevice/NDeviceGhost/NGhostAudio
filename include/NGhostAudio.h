#ifndef NGHOSTAUDIO_PROTECT
#define NGHOSTAUDIO_PROTECT

// ---------------------
// namespace NGhostAudio
// ---------------------

// namespace NLib
#include "../../../../NLib/NLib/NLib/include/NLib/NLib.h"

// __::Version
#include "__/__NGhostAudio_Version.h"

// namespace NHTTP
#include "../../../../NLib/NHTTP/include/NHTTP.h"

// namespace NParser
#include "../../../../NParser/NParser/include/NParser.h"

// namespace NJson
#include "../../../../NParser/NJson/include/NJson.h"

// namespace NDeviceCommon
#include "../../../NDeviceCommon/include/NDeviceCommon.h"

// namespace NGhostCommon
#include "../../NGhostCommon/include/NGhostCommon.h"

// namespace NGhostAudio::Configuration
#include "Configuration/NGhostAudio_Configuration.h"

// namespace NGhostAudio::Audio
#include "Audio/NGhostAudio_Audio.h"

// namespace NGhostAudio::Service
#include "Service/NGhostAudio_Service.h"

// struct NGhostAudio::NGhostAudio
#include "NGhostAudio_NGhostAudio.h"

// namespace NGhostAudio::Image
#include "Image/NGhostAudio_Image.h"

// namespace NGhostAudio::Action
#include "Action/NGhostAudio_Action.h"

#endif // !NGHOSTAUDIO_PROTECT
