#ifndef NGHOSTAUDIO_CONFIGURATION_NGHOSTAUDIOCONFIGURATION_PROTECT
#define NGHOSTAUDIO_CONFIGURATION_NGHOSTAUDIOCONFIGURATION_PROTECT

// -----------------------------------------------------------
// struct NGhostAudio::Configuration::NGhostAudioConfiguration
// -----------------------------------------------------------

typedef struct NGhostAudioConfiguration
{
	/**
	 * Device name
	 */
	char *m_deviceName;

	/**
	 * Network
	 */
	// Listening port
	NU32 m_networkListeningPort;

	// Trusted token IP
	NGhostTrustedTokenIPList *m_trustedTokenIPList;

	/**
	 * Authentication
	 */
	// Username
	char *m_authenticationUsername;

	// Password
	char *m_authenticationPassword;

	/**
	 * Device UUID
	 */
	char *m_deviceUUID;

	/**
	 * Build informations
	 */
	char m_buildDetail[ 32 ];
} NGhostAudioConfiguration;

/**
 * Build configuration from file
 *
 * @param configurationFilePath
 * 		The configuration file path
 *
 * @return the configuration instance
 */
__ALLOC NGhostAudioConfiguration *NGhostAudio_Configuration_NGhostAudioConfiguration_Build( const char *configurationFilePath );

/**
 * Build default configuration
 *
 * @return the default configuration
 */
__ALLOC NGhostAudioConfiguration *NGhostAudio_Configuration_NGhostAudioConfiguration_Build2( void );

/**
 * Destroy configuration
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Configuration_NGhostAudioConfiguration_Destroy( NGhostAudioConfiguration** );

/**
 * Get device name
 *
 * @param this
 * 		This instance
 *
 * @return the device name
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetDeviceName( const NGhostAudioConfiguration* );

/**
 * Get device uuid
 *
 * @param this
 * 		This instance
 *
 * @return the device uuid
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetDeviceUUID( const NGhostAudioConfiguration* );

/**
 * Get build details
 *
 * @param this
 * 		This instance
 *
 * @return the build details
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetBuildDetail( const NGhostAudioConfiguration* );

/**
 * Get network listening port
 *
 * @param this
 * 		This instance
 *
 * @return the listening port
 */
NU32 NGhostAudio_Configuration_NGhostAudioConfiguration_GetNetworkListeningPort( const NGhostAudioConfiguration* );

/**
 * Get authentication username
 *
 * @param this
 * 		This instance
 *
 * @return the username
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetAuthenticationUsername( const NGhostAudioConfiguration* );

/**
 * Get authentication password
 *
 * @param this
 * 		This instance
 *
 * @return the password
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetAuthenticationPassword( const NGhostAudioConfiguration* );

/**
 * Get trusted token ip list
 *
 * @param this
 * 		This instance
 *
 * @return the trusted token ip list
 */
const NGhostTrustedTokenIPList *NGhostAudio_Configuration_NGhostAudioConfiguration_GetTrustedTokenIPList( const NGhostAudioConfiguration* );

/**
 * Save configuration
 *
 * @param this
 * 		This instance
 * @param configurationFilePath
 *		The configuration file path
 *
 * @return if operation succeeded
 */
NBOOL NGhostAudio_Configuration_NGhostAudioConfiguration_Save( const NGhostAudioConfiguration*,
	const char *configurationFilePath );

#endif // !NGHOSTAUDIO_CONFIGURATION_NGHOSTAUDIOCONFIGURATION_PROTECT
