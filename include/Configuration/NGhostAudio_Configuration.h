#ifndef NGHOSTAUDIO_CONFIGURATION_PROTECT
#define NGHOSTAUDIO_CONFIGURATION_PROTECT

// ------------------------------------
// namespace NGhostAudio::Configuration
// ------------------------------------

// enum NGhostAudio::Configuration::NGhostAudioConfigurationProperty
#include "NGhostAudio_Configuration_NGhostAudioConfigurationProperty.h"

// struct NGhostAudio::Configuration::NGhostAudioConfiguration
#include "NGhostAudio_Configuration_NGhostAudioConfiguration.h"

#endif // !NGHOSTAUDIO_CONFIGURATION_PROTECT
