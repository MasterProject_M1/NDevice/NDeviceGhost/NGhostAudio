#ifndef NGHOSTAUDIO_CONFIGURATION_NGHOSTAUDIOCONFIGURATIONPROPERTY_PROTECT
#define NGHOSTAUDIO_CONFIGURATION_NGHOSTAUDIOCONFIGURATIONPROPERTY_PROTECT

// -----------------------------------------------------------------
// enum NGhostAudio::Configuration::NGhostAudioConfigurationProperty
// -----------------------------------------------------------------

typedef enum NGhostAudioConfigurationProperty
{
	// Device name
	NGHOST_AUDIO_CONFIGURATION_PROPERTY_DEVICE_NAME,

	// Network
	NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT,
	NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_TRUSTED_TOKEN_IP,

	// Authentication
	NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME,
	NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD,

	NGHOST_AUDIO_CONFIGURATION_PROPERTIES
} NGhostAudioConfigurationProperty;

#define NGHOST_AUDIO_CONFIGURATION_PROPERTY_DEVICE_NAME_DEFAULT_VALUE					"ToBeSet"
#define NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT_DEFAULT_VALUE		NDEVICE_COMMON_TYPE_GHOST_AUDIO_LISTENING_PORT
#define NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_TRUSTED_TOKEN_IP_DEFAULT_VALUE		""
#define NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME_DEFAULT_VALUE		"test"
#define NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD_DEFAULT_VALUE		"test"

/**
 * Get configuration property name
 *
 * @param property
 * 		The property
 *
 * @return the property name
 */
const char *NGhostAudio_Configuration_NGhostAudioConfigurationProperty_GetPropertyName( NGhostAudioConfigurationProperty property );

/**
 * Build property list
 *
 * @return the property list
 */
__ALLOC char **NGhostAudio_Configuration_NGhostAudioConfigurationProperty_BuildAllPropertyList( void );

#ifdef NGHOSTAUDIO_CONFIGURATION_NGHOSTAUDIOCONFIGURATIONPROPERTY_INTERNE
static const char NGhostAudioConfigurationPropertyText[ NGHOST_AUDIO_CONFIGURATION_PROPERTIES ][ 32 ] =
{
	"deviceName:",

	"listeningPort:",
	"trustedTokenIP:",

	"username:",
	"password:"
};
#endif // NGHOSTAUDIO_CONFIGURATION_NGHOSTAUDIOCONFIGURATIONPROPERTY_INTERNE

#endif // !NGHOSTAUDIO_CONFIGURATION_NGHOSTAUDIOCONFIGURATIONPROPERTY_PROTECT
