#ifndef NGHOSTAUDIO_NGHOSTAUDIO_PROTECT
#define NGHOSTAUDIO_NGHOSTAUDIO_PROTECT

// -------------------------------
// struct NGhostAudio::NGhostAudio
// -------------------------------

typedef struct NGhostAudio
{
	// Configuration
	NGhostAudioConfiguration *m_configuration;

	// Common
	NGhostCommon *m_common;

	// Is running
	NBOOL m_isRunning;

	// Monitoring agent
	NMonitoringAgent *m_monitoringAgent;

	// Ghost audio player
	NGhostAudioPlayer *m_ghostAudioPlayer;

	// Action updater
	NGhostActionUpdater *m_actionUpdater;
} NGhostAudio;

/**
 * Build instance
 *
 * @param configurationFilePath
 * 		The configuration file path
 *
 * @return the ghost audio instance
 */
__ALLOC NGhostAudio *NGhostAudio_NGhostAudio_Build( const char *configurationFilePath );

/**
 * Destroy instance
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_NGhostAudio_Destroy( NGhostAudio** );

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the configuration
 */
const NGhostAudioConfiguration *NGhostAudio_NGhostAudio_GetConfiguration( const NGhostAudio* );

/**
 * Is running?
 *
 * @param this
 * 		This instance
 *
 * @return if is running
 */
NBOOL NGhostAudio_NGhostAudio_IsRunning( const NGhostAudio* );

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NGhostAudio_NGhostAudio_GetIP( const NGhostAudio* );

/**
 * Get audio player
 *
 * @param this
 * 		This instance
 *
 * @return the audio player
 */
const NGhostAudioPlayer *NGhostAudio_NGhostAudio_GetAudioPlayer( const NGhostAudio* );

/**
 * Get ghost common
 *
 * @param this
 * 		This instance
 *
 * @return the ghost common
 */
NGhostCommon *NGhostAudio_NGhostAudio_GetGhostCommon( const NGhostAudio* );

#endif // !NGHOSTAUDIO_NGHOSTAUDIO_PROTECT
