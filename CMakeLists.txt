# Set minimal cmake version
cmake_minimum_required( VERSION
		3.1.3 )

# Dossier de sortie
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY
		${CMAKE_BINARY_DIR}/../Output )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY
		${CMAKE_BINARY_DIR}/../Output )
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY
		${CMAKE_BINARY_DIR}/../Output )

# Source files NLib
file( GLOB_RECURSE
		NLIB_SOURCE_FILES
		../../../NLib/NLib/NLib/src/*.c )
file( GLOB_RECURSE
		NLIB_HEADER_FILES
		../../../NLib/NLib/NLib/include/*.h )

# Source files HTTP
file( GLOB_RECURSE
		NHTTP_SOURCE_FILES
		../../../NLib/NHTTP/src/*.c )
file( GLOB_RECURSE
		NHTTP_HEADER_FILES
		../../../NLib/NHTTP/include/*.h )

# Source files device common
file( GLOB_RECURSE
		NDEVICE_COMMON_SOURCE_FILES
		../../NDeviceCommon/src/*.c )
file( GLOB_RECURSE
		NDEVICE_COMMON_HEADER_FILES
		../../NDeviceCommon/include/*.h )

# Source files NParser
file( GLOB_RECURSE
		NPARSER_SOURCE_FILES
		../../../NParser/NParser/src/*.c )
file( GLOB_RECURSE
		NPARSER_HEADER_FILES
		../../../NParser/NParser/include/*.h )

# Source files JSon parser
file( GLOB_RECURSE
		NJSON_PARSER_SOURCE_FILES
		../../../NParser/NJson/src/*.c )
file( GLOB_RECURSE
		NJSON_PARSER_HEADER_FILES
		../../../NParser/NJson/include/*.h )

# Source files ghost common
file( GLOB_RECURSE
		NGHOST_COMMON_SOURCE_FILES
		../NGhostCommon/src/*.c )
file( GLOB_RECURSE
		NGHOST_COMMON_HEADER_FILES
		../NGhostCommon/include/*.h )

# Source files ghost audio
file( GLOB_RECURSE
		NGHOST_AUDIO_SOURCE_FILES
		src/*.c )
file( GLOB_RECURSE
		NGHOST_AUDIO_HEADER_FILES
		include/*.h )

# Ghost audio
project( NGHOSTAUDIO_PROJECT )
	# Ghost audio source
	add_executable( NGhostAudio
		${NLIB_SOURCE_FILES}
		${NLIB_HEADER_FILES}
		${NHTTP_SOURCE_FILES}
		${NHTTP_HEADER_FILES}
		${NDEVICE_COMMON_SOURCE_FILES}
		${NDEVICE_COMMON_HEADER_FILES}
		${NJSON_PARSER_SOURCE_FILES}
		${NJSON_PARSER_HEADER_FILES}
		${NPARSER_SOURCE_FILES}
		${NPARSER_HEADER_FILES}
		${NGHOST_COMMON_SOURCE_FILES}
		${NGHOST_COMMON_HEADER_FILES}
		${NGHOST_AUDIO_SOURCE_FILES}
		${NGHOST_AUDIO_HEADER_FILES} )

	# NLib network module
	target_compile_definitions( NGhostAudio
		PUBLIC
		NLIB_MODULE_RESEAU )

	# NLib network module
	target_compile_definitions( NGhostAudio
		PUBLIC
		NLIB_MODULE_REPERTOIRE )

	# NLib FMod module
	target_compile_definitions( NGhostAudio
		PUBLIC
		NLIB_MODULE_FMODEX )

	# NLib SDL2 modules
	#target_compile_definitions( NGhostAudio
	#	PUBLIC
	#	NLIB_MODULE_SDL )
	#target_compile_definitions( NGhostAudio
	#	PUBLIC
	#	NLIB_MODULE_SDL_IMAGE )
	#target_compile_definitions( NGhostAudio
	#	PUBLIC
	#	NLIB_MODULE_SDL_TTF )

	# NLib SSH module
	target_compile_definitions( NGhostAudio
		PUBLIC
		NLIB_MODULE_SSH )

	# Link
	target_link_libraries( NGhostAudio
		pthread )
	target_link_libraries( NGhostAudio
		fmod )
	target_link_libraries( NGhostAudio
		m )
	target_link_libraries( NGhostAudio
		ssh2 )
	target_link_libraries( NGhostAudio
		crypto )
	target_link_libraries( NGhostAudio
		SDL2 )
	target_link_libraries( NGhostAudio
		SDL2_image )
	target_link_libraries( NGhostAudio
		SDL2_ttf )
