Ghost audio
===========

Introduction
------------

This project is a simple REST control capable audio player.

It will have the ability to synchronize itself with other ghost audio devices
running on other remote locations.

Audio player
------------

> Add a new audio object to be read

`POST /ghost/audio `

> Will look for file at 127.0.0.1:1234/path/to/file on remote side
> Authentication manager is supposed to own details on method to
> use (HTTPBasic or SFTP) and login details

```json
{
	"name": "UniqueNameWithoutSpace", # Required (no space, no dot, = song ID)
	"isLoop": true, # Optionnal (default true)
	"synchList": # Optionnal, synchronized start/pause/position changes
	[
		"10.0.0.1",
		"10.0.0.2"
	],
		
	"remote": # Required
	{
		"hostname": "127.0.0.1",
		"port": "1234",
		"filePath": "/path/to/file", # Required
	},
	
	"state":
	{
		"isPaused": true, # Optionnal (default true)
		"volume": 100, # Optionnal (default 100)
		"position": 450 # Optionnal (default 0ms)
	}
}
```

`Start a track:`

```bash
curl test:test@127.0.0.1:16561/ghost/audio/ -X POST -d "{ \"name\": \"IceRink\", \"remote\": { \"hostname\": \"127.0.0.1\", \"port\": 22, \"filePath\": \"/www/Music/IceRink.mp3\" } }"
```

`Start a track with synchronization:`

```bash
curl test:test@127.0.0.1:16561/ghost/audio/ -X POST -d "{ \"name\": \"IceRink\", \"remote\": { \"hostname\": \"192.168.1.23\", \"port\": 22, \"filePath\": \"/www/Music/IceRink.mp3\" }, \"synchList\": [ \"192.168.1.23\", \"192.168.1.17\" ], \"state\": { \"isPaused\": false } }"
```

> Edit track status

# More than one property in a request

`PUT /ghost/audio/{{ name }}/state`

```json
{
	"volume": 50, # Optionnal
	"isPaused": false, # Optionnal
	"position": 10 # Optionnal (ms)
}
```

```bash
curl test:test@127.0.0.1:16561/ghost/audio/{{ name }}/state -X PUT -d "{ \"isPaused\": false, \"volume\": 50, \"position\": 10 }"
```

# Set pause active

`PUT /ghost/audio/{{ name }}/state/isPaused`

```json
{ "isPaused": true }
```

```bash
curl test:test@127.0.0.1:16561/ghost/audio/{{ name }}/state/isPaused -X PUT -d "{ \"isPaused\": true }"
```

> Delete a track

`DELETE /ghost/audio/{{ name }}`

> Delete all tracks
`DELETE /ghost/audio/`

Dependencies
------------

- NLib
- NHTTP
- NGhostCommon
- NParser
- NJson
- NDeviceCommon
- NDeviceHUE
- NDeviceScanner

- FMod
- libssh2 (compiled by hand)

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/NDevice/NDeviceGhost/NGhostAudio.git
