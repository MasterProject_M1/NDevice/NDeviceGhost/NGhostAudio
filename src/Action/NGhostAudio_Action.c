#include "../../include/NGhostAudio.h"

// -----------------------------
// namespace NGhostAudio::Action
// -----------------------------

/**
 * Update actions
 *
 * @param ghostAudio
 * 		The ghost audio
 * @param actionList
 * 		The action list
 * @param ghostCommon
 * 		The ghost common
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostAudio_Action_UpdateAction( NGhostAudio *ghostAudio,
	NGhostActionList *actionList,
	const NGhostCommon *ghostCommon )
{
	// Action
	NGhostAction *action;

	// Sentence
	char sentence[ 256 ];

	// Parser output list
	NParserOutputList *parserOutputList;

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Fill parser output list
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGHOSTAUDIO_TRACK_REMOTE_SERVICE_HOSTNAME ),
		"#1#" );
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGHOSTAUDIO_TRACK_REMOTE_SERVICE_PORT ),
		"#2#" );
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGHOSTAUDIO_TRACK_REMOTE_SERVICE_FILE_PATH ),
		"#3#" );
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGHOST_AUDIO_TRACK_STATE_SERVICE_VOLUME ),
		"#4#" );
	NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED ),
		NFALSE );
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackService_GetName( NGHOST_AUDIO_TRACK_SERVICE_NAME ),
		"#5#" );
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackService_GetName( NGHOST_AUDIO_TRACK_SERVICE_IS_LOOP ),
		"#6#" );

	// Build sentence
	snprintf( sentence,
		256,
		NGhostCommon_Action_NGhostActionType_GetSentence( NGHOST_ACTION_TYPE_AUDIO_PLAY_SOUND ),
		NGhostCommon_Service_Info_NGhostInfo_GetDeviceName( NGhostCommon_NGhostCommon_GetDeviceInfo( ghostCommon ) ) );

	// Build play sound action
	if( !( action = NGhostCommon_Action_NGhostAction_Build( "127.0.0.1",
		NGhostAudio_Configuration_NGhostAudioConfiguration_GetNetworkListeningPort( NGhostAudio_NGhostAudio_GetConfiguration( ghostAudio ) ),
		NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE,
		NTYPE_REQUETE_HTTP_POST,
		parserOutputList,
		NGHOST_ACTION_TYPE_AUDIO_PLAY_SOUND,
		sentence,
		NGhostCommon_Service_Info_NGhostInfo_GetDeviceUUID( NGhostCommon_NGhostCommon_GetDeviceInfo( ghostCommon ) ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add action
	NGhostCommon_Action_NGhostActionList_AddAction( actionList,
		action );

	// Build sentence
	snprintf( sentence,
		256,
		NGhostCommon_Action_NGhostActionType_GetSentence( NGHOST_ACTION_TYPE_AUDIO_STOP_ALL_SOUND ),
		NGhostCommon_Service_Info_NGhostInfo_GetDeviceName( NGhostCommon_NGhostCommon_GetDeviceInfo( ghostCommon ) ) );

	// Build destroy all sound action
	if( !( action = NGhostCommon_Action_NGhostAction_Build( "127.0.0.1",
		NGhostAudio_Configuration_NGhostAudioConfiguration_GetNetworkListeningPort( NGhostAudio_NGhostAudio_GetConfiguration( ghostAudio ) ),
		NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE,
		NTYPE_REQUETE_HTTP_DELETE,
		NULL,
		NGHOST_ACTION_TYPE_AUDIO_STOP_ALL_SOUND,
		sentence,
		NGhostCommon_Service_Info_NGhostInfo_GetDeviceUUID( NGhostCommon_NGhostCommon_GetDeviceInfo( ghostCommon ) ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add action
	NGhostCommon_Action_NGhostActionList_AddAction( actionList,
		action );

	// OK
	return NTRUE;
}

/**
 * Update actions
 *
 * @param actionUpdater
 * 		The action updater
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Action_UpdateThread( NGhostActionUpdater *actionUpdater )
{
	// Action list
	NGhostActionList *actionList;

	// Ghost audio
	NGhostAudio *ghostAudio;

	// Ghost common
	NGhostCommon *ghostCommon;

	// Ghost device status
	const NGhostDeviceStatus *deviceStatus;

	// Referal details
	NGhostStatusReferal *statusReferal;

	// Client list
	const NListe *ghostClientList;

	// Authentication entry
	const NAuthenticationEntry *authenticationEntry;

	// Ghost client service
	NGhostStatusServiceReferal *serviceReferal;

	// Authentication manager
	NAuthenticationManager *authenticationManager;

	// Iterator
	NU32 i;

	// Get action list
	if( !( actionList = NGhostCommon_Action_NGhostActionUpdater_GetActionList( actionUpdater ) )
		|| !( ghostAudio = (NGhostAudio*)NGhostCommon_Action_NGhostActionUpdater_GetDevice( actionUpdater ) )
		|| !( ghostCommon = NGhostAudio_NGhostAudio_GetGhostCommon( ghostAudio ) )
		|| !( deviceStatus = NGhostCommon_NGhostCommon_GetDeviceStatus( ghostCommon ) )
		|| !( statusReferal = NGhostCommon_Service_Status_NGhostDeviceStatus_GetGhostStatusReferal( deviceStatus ) )
		|| !( ghostClientList = NGhostCommon_Service_Status_Referal_NGhostStatusReferal_GetClientIPList( statusReferal ) )
		|| !( authenticationManager = NGhostCommon_NGhostCommon_GetAuthenticationManager( ghostCommon ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	do
	{
		// Lock action list
		NGhostCommon_Action_NGhostAction_ActivateProtection( actionList );

		// Update action
		if( !NGhostAudio_Action_UpdateAction( ghostAudio,
			actionList,
			ghostCommon ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_INIT_UNCOMPLETE );

			// Deactivate protection
			NGhostCommon_Action_NGhostAction_DeactivateProtection( actionList );

			// Quit
			return NFALSE;
		}

		// Lock authentication manager
		NDeviceCommon_Authentication_NAuthenticationManager_ActivateProtection( authenticationManager );

		// Lock status referal
		NGhostCommon_Service_Status_Referal_NGhostStatusReferal_ActivateProtection( statusReferal );

		// Iterate client IP
		for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( ghostClientList ); i++ )
			if( ( serviceReferal = (NGhostStatusServiceReferal *)NLib_Memoire_NListe_ObtenirElementDepuisIndex( ghostClientList,
				i ) ) != NULL )
				// Find authentication entry
				if( ( authenticationEntry = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForHostnameAndPort( authenticationManager,
					NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetIP( serviceReferal ),
					NDeviceCommon_Type_NDeviceType_GetListeningPort( NDEVICE_TYPE_GHOST_CLIENT ) ) ) != NULL )
					// Share action list
					NGhostCommon_Action_NGhostActionList_ShareActionList( actionList,
						NGhostCommon_Service_Status_Referal_NGhostStatusServiceReferal_GetHTTPClient( serviceReferal ),
						authenticationEntry,
						NGhostCommon_NGhostCommon_GetIP( ghostCommon ) );

		// Unlock status referal
		NGhostCommon_Service_Status_Referal_NGhostStatusReferal_DeactivateProtection( statusReferal );

		// Unlock authentication manager
		NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( authenticationManager );

		// Unlock action list
		NGhostCommon_Action_NGhostAction_DeactivateProtection( actionList );

		// Wait
		NLib_Temps_Attendre( NGHOST_AUDIO_DELAY_BETWEEN_UPDATE );
	} while( NGhostCommon_Action_NGhostActionUpdater_IsUpdateThreadRunning( actionUpdater ) );

	// OK
	return NTRUE;
}
