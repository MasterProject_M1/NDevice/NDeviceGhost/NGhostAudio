#include "../include/NGhostAudio.h"

// -------------------------------
// struct NGhostAudio::NGhostAudio
// -------------------------------

/**
 * Build instance
 *
 * @param configurationFilePath
 * 		The configuration file path
 *
 * @return the ghost audio instance
 */
__ALLOC NGhostAudio *NGhostAudio_NGhostAudio_Build( const char *configurationFilePath )
{
	// Output
	__OUTPUT NGhostAudio *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAudio ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Try to load configuration
	if( !( out->m_configuration = NGhostAudio_Configuration_NGhostAudioConfiguration_Build( configurationFilePath ) ) )
	{
		// Try build default configuration
		if( !( out->m_configuration = NGhostAudio_Configuration_NGhostAudioConfiguration_Build2( ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Free
			NFREE( out );

			// Quit
			return NULL;
		}

		// Save default configuration
		NGhostAudio_Configuration_NGhostAudioConfiguration_Save( out->m_configuration,
			configurationFilePath );
	}

	// Build common
	if( !( out->m_common = NGhostCommon_NGhostCommon_Build( NULL,
		NGhostAudio_Configuration_NGhostAudioConfiguration_GetNetworkListeningPort( out->m_configuration ),
		NGhostAudio_Service_ProcessRequest,
		NDeviceCommon_Type_NDeviceType_GetName( NDEVICE_TYPE_GHOST_AUDIO ),
		out,
		NDEVICE_TYPE_GHOST_AUDIO,
		NTRUE,
		NGhostAudio_Configuration_NGhostAudioConfiguration_GetDeviceName( out->m_configuration ),
		NGhostAudio_Configuration_NGhostAudioConfiguration_GetAuthenticationUsername( out->m_configuration ),
		NGhostAudio_Configuration_NGhostAudioConfiguration_GetAuthenticationPassword( out->m_configuration ),
		NGHOST_VERSION_MAJOR,
		NGHOST_VERSION_MINOR,
		NGhostAudio_Configuration_NGhostAudioConfiguration_GetTrustedTokenIPList( out->m_configuration ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy configuration
		NGhostAudio_Configuration_NGhostAudioConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build monitoring agent
	out->m_isRunning = NTRUE;
	if( !( out->m_monitoringAgent = NGhostCommon_Monitoring_NMonitoringAgent_Build( NGhostCommon_NGhostCommon_GetTokenManager( out->m_common ),
		out,
		&out->m_isRunning,
		NULL ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy common
		NGhostCommon_NGhostCommon_Destroy( &out->m_common );

		// Destroy configuration
		NGhostAudio_Configuration_NGhostAudioConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build audio player
	if( !( out->m_ghostAudioPlayer = NGhostAudio_Audio_NGhostAudioPlayer_Build( NGhostCommon_NGhostCommon_GetAuthenticationManager( out->m_common ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy monitoring agent
		NGhostCommon_Monitoring_NMonitoringAgent_Destroy( &out->m_monitoringAgent );

		// Destroy common
		NGhostCommon_NGhostCommon_Destroy( &out->m_common );

		// Destroy configuration
		NGhostAudio_Configuration_NGhostAudioConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build action updater
	if( !( out->m_actionUpdater = NGhostCommon_Action_NGhostActionUpdater_Build( NGhostCommon_NGhostCommon_GetActionList( out->m_common ),
		out,
		NGhostAudio_Action_UpdateThread ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy audio player
		NGhostAudio_Audio_NGhostAudioPlayer_Destroy( &out->m_ghostAudioPlayer );

		// Destroy monitoring agent
		NGhostCommon_Monitoring_NMonitoringAgent_Destroy( &out->m_monitoringAgent );

		// Destroy common
		NGhostCommon_NGhostCommon_Destroy( &out->m_common );

		// Destroy configuration
		NGhostAudio_Configuration_NGhostAudioConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy instance
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_NGhostAudio_Destroy( NGhostAudio **this )
{
	// Destroy action updater
	NGhostCommon_Action_NGhostActionUpdater_Destroy( &(*this)->m_actionUpdater );

	// Destroy audio player
	NGhostAudio_Audio_NGhostAudioPlayer_Destroy( &(*this)->m_ghostAudioPlayer );

	// Destroy monitoring agent
	NGhostCommon_Monitoring_NMonitoringAgent_Destroy( &(*this)->m_monitoringAgent );

	// Destroy common data
	NGhostCommon_NGhostCommon_Destroy( &(*this)->m_common );

	// Destroy configuration
	NGhostAudio_Configuration_NGhostAudioConfiguration_Destroy( &(*this)->m_configuration );

	// Free
	NFREE( (*this) );
}

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the configuration
 */
const NGhostAudioConfiguration *NGhostAudio_NGhostAudio_GetConfiguration( const NGhostAudio *this )
{
	return this->m_configuration;
}

/**
 * Is running?
 *
 * @param this
 * 		This instance
 *
 * @return if is running
 */
NBOOL NGhostAudio_NGhostAudio_IsRunning( const NGhostAudio *this )
{
	return this->m_isRunning;
}

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NGhostAudio_NGhostAudio_GetIP( const NGhostAudio *this )
{
	return NGhostCommon_NGhostCommon_GetIP( this->m_common );
}

/**
 * Get audio player
 *
 * @param this
 * 		This instance
 *
 * @return the audio player
 */
const NGhostAudioPlayer *NGhostAudio_NGhostAudio_GetAudioPlayer( const NGhostAudio *this )
{
	return this->m_ghostAudioPlayer;
}

/**
 * Get ghost common
 *
 * @param this
 * 		This instance
 *
 * @return the ghost common
 */
NGhostCommon *NGhostAudio_NGhostAudio_GetGhostCommon( const NGhostAudio *this )
{
	return this->m_common;
}
