#define NGHOSTAUDIO_CONFIGURATION_NGHOSTAUDIOCONFIGURATIONPROPERTY_INTERNE
#include "../../include/NGhostAudio.h"

// -----------------------------------------------------------------
// enum NGhostAudio::Configuration::NGhostAudioConfigurationProperty
// -----------------------------------------------------------------

/**
 * Get configuration property name
 *
 * @param property
 * 		The property
 *
 * @return the property name
 */
const char *NGhostAudio_Configuration_NGhostAudioConfigurationProperty_GetPropertyName( NGhostAudioConfigurationProperty property )
{
	return NGhostAudioConfigurationPropertyText[ property ];
}

/**
 * Build property list
 *
 * @return the property list
 */
__ALLOC char **NGhostAudio_Configuration_NGhostAudioConfigurationProperty_BuildAllPropertyList( void )
{
	// Output
	__OUTPUT char **out;

	// Iterator
	NU32 i;

	// Allocate memory
	if( !( out = calloc( NGHOST_AUDIO_CONFIGURATION_PROPERTIES,
		sizeof( char* ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate properties
	for( i = 0; i < NGHOST_AUDIO_CONFIGURATION_PROPERTIES; i++ )
		// Duplicate
		if( !( out[ i ] = NLib_Chaine_Dupliquer( NGhostAudioConfigurationPropertyText[ i ] ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_COPY );

			// Free
			for( i--; (NS32)i >= 0; i-- )
				NFREE( out[ i ] );
			NFREE( out );

			// Quit
			return NULL;
		}

	// OK
	return out;
}

