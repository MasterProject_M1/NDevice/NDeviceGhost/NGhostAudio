#include "../../include/NGhostAudio.h"

// -----------------------------------------------------------
// struct NGhostAudio::Configuration::NGhostAudioConfiguration
// -----------------------------------------------------------

/**
 * Build configuration from file
 *
 * @param configurationFilePath
 * 		The configuration file path
 *
 * @return the configuration instance
 */
__ALLOC NGhostAudioConfiguration *NGhostAudio_Configuration_NGhostAudioConfiguration_Build( const char *configurationFilePath )
{
	// Output
	__OUTPUT NGhostAudioConfiguration *out;

	// Properties list
	char **propertyList;

	// File
	NFichierClef *file;

	// Iterator
	NU32 i;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAudioConfiguration ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build properties list
	if( !( propertyList = NGhostAudio_Configuration_NGhostAudioConfigurationProperty_BuildAllPropertyList( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build file
	if( !( file = NLib_Fichier_Clef_NFichierClef_Construire( configurationFilePath,
		(const char**)propertyList,
		NGHOST_AUDIO_CONFIGURATION_PROPERTIES,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Free
		for( i = 0; i < NGHOST_AUDIO_CONFIGURATION_PROPERTIES; i++ )
			NFREE( propertyList[ i ] );
		NFREE( propertyList );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Free
	for( i = 0; i < NGHOST_AUDIO_CONFIGURATION_PROPERTIES; i++ )
		NFREE( propertyList[ i ] );
	NFREE( propertyList );

	// Get informations
	NGhostClient_Helper_BuildVersionShort( out->m_buildDetail );
	if( !( out->m_deviceUUID = NGhostCommon_Hardware_GetUniqueID( NDEVICE_TYPE_GHOST_AUDIO ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Extract integer properties
	if( ( out->m_networkListeningPort = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( file,
		NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT ) ) == NERREUR )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

		// Close file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Free
		NFREE( out->m_deviceUUID );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Extract string properties
	if( !( out->m_deviceName = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
			NGHOST_AUDIO_CONFIGURATION_PROPERTY_DEVICE_NAME ) )
		|| !( out->m_authenticationUsername = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
			NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME ) )
		|| !( out->m_authenticationPassword = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
			NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Close file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Free
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_deviceName );
		NFREE( out->m_deviceUUID );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build trusted token list
	if( !( out->m_trustedTokenIPList = NGhostCommon_Token_NGhostTrustedTokenIPList_Build( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( file,
		NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_TRUSTED_TOKEN_IP ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Close file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Free
		NFREE( out->m_authenticationPassword );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_deviceName );
		NFREE( out->m_deviceUUID );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Close file
	NLib_Fichier_Clef_NFichierClef_Detruire( &file );

	// OK
	return out;
}

/**
 * Build default configuration
 *
 * @return the default configuration
 */
__ALLOC NGhostAudioConfiguration *NGhostAudio_Configuration_NGhostAudioConfiguration_Build2( void )
{
	// Output
	__OUTPUT NGhostAudioConfiguration *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAudioConfiguration ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate
	if( !( out->m_deviceName = NLib_Chaine_Dupliquer( NGHOST_AUDIO_CONFIGURATION_PROPERTY_DEVICE_NAME_DEFAULT_VALUE ) )
		|| !( out->m_authenticationUsername = NLib_Chaine_Dupliquer( NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME_DEFAULT_VALUE ) )
		|| !( out->m_authenticationPassword = NLib_Chaine_Dupliquer( NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD_DEFAULT_VALUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_deviceName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build trusted token IP
	if( !( out->m_trustedTokenIPList = NGhostCommon_Token_NGhostTrustedTokenIPList_Build( NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_TRUSTED_TOKEN_IP_DEFAULT_VALUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_authenticationPassword );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_deviceName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Get informations
	NGhostClient_Helper_BuildVersionShort( out->m_buildDetail );
	if( !( out->m_deviceUUID = NGhostCommon_Hardware_GetUniqueID( NDEVICE_TYPE_GHOST_AUDIO ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &out->m_trustedTokenIPList );

		// Free
		NFREE( out->m_authenticationPassword );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_deviceName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Copy integer values
	out->m_networkListeningPort = NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT_DEFAULT_VALUE;

	// OK
	return out;
}

/**
 * Destroy configuration
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Configuration_NGhostAudioConfiguration_Destroy( NGhostAudioConfiguration **this )
{
	// Destroy
	NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &(*this)->m_trustedTokenIPList );

	// Free
	NFREE( (*this)->m_authenticationUsername );
	NFREE( (*this)->m_authenticationPassword );
	NFREE( (*this)->m_deviceName );
	NFREE( (*this)->m_deviceUUID );
	NFREE( (*this) );
}

/**
 * Get device name
 *
 * @param this
 * 		This instance
 *
 * @return the device name
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetDeviceName( const NGhostAudioConfiguration *this )
{
	return this->m_deviceName;
}

/**
 * Get device uuid
 *
 * @param this
 * 		This instance
 *
 * @return the device uuid
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetDeviceUUID( const NGhostAudioConfiguration *this )
{
	return this->m_deviceUUID;
}

/**
 * Get build details
 *
 * @param this
 * 		This instance
 *
 * @return the build details
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetBuildDetail( const NGhostAudioConfiguration *this )
{
	return this->m_buildDetail;
}

/**
 * Get network listening port
 *
 * @param this
 * 		This instance
 *
 * @return the listening port
 */
NU32 NGhostAudio_Configuration_NGhostAudioConfiguration_GetNetworkListeningPort( const NGhostAudioConfiguration *this )
{
	return this->m_networkListeningPort;
}

/**
 * Get authentication username
 *
 * @param this
 * 		This instance
 *
 * @return the username
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetAuthenticationUsername( const NGhostAudioConfiguration *this )
{
	return this->m_authenticationUsername;
}

/**
 * Get authentication password
 *
 * @param this
 * 		This instance
 *
 * @return the password
 */
const char *NGhostAudio_Configuration_NGhostAudioConfiguration_GetAuthenticationPassword( const NGhostAudioConfiguration *this )
{
	return this->m_authenticationPassword;
}

/**
 * Get trusted token ip list
 *
 * @param this
 * 		This instance
 *
 * @return the trusted token ip list
 */
const NGhostTrustedTokenIPList *NGhostAudio_Configuration_NGhostAudioConfiguration_GetTrustedTokenIPList( const NGhostAudioConfiguration *this )
{
	return this->m_trustedTokenIPList;
}

/**
 * Save configuration
 *
 * @param this
 * 		This instance
 * @param configurationFilePath
 *		The configuration file path
 *
 * @return if operation succeeded
 */
NBOOL NGhostAudio_Configuration_NGhostAudioConfiguration_Save( const NGhostAudioConfiguration *this,
	const char *configurationFilePath )
{
	// File
	NFichierTexte *file;

	// Exported data
	char *export;

	// Build file
	if( !( file = NLib_Fichier_NFichierTexte_ConstruireEcriture( configurationFilePath,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Write device
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"[Device]\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostAudio_Configuration_NGhostAudioConfigurationProperty_GetPropertyName( NGHOST_AUDIO_CONFIGURATION_PROPERTY_DEVICE_NAME ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		this->m_deviceName );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n\n" );

	// Write network
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"[Network]\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostAudio_Configuration_NGhostAudioConfigurationProperty_GetPropertyName( NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	NLib_Fichier_NFichierTexte_Ecrire( file,
		this->m_networkListeningPort );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		'\n' );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostAudio_Configuration_NGhostAudioConfigurationProperty_GetPropertyName( NGHOST_AUDIO_CONFIGURATION_PROPERTY_NETWORK_TRUSTED_TOKEN_IP ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	if( ( export = NGhostCommon_Token_NGhostTrustedTokenIPList_Export( this->m_trustedTokenIPList ) ) != NULL )
	{
		NLib_Fichier_NFichierTexte_Ecrire3( file,
			export );
		NFREE( export );
	}
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n\n" );

	// Write authentication
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"[Authentication]\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostAudio_Configuration_NGhostAudioConfigurationProperty_GetPropertyName( NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		this->m_authenticationUsername );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		'\n' );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostAudio_Configuration_NGhostAudioConfigurationProperty_GetPropertyName( NGHOST_AUDIO_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		this->m_authenticationPassword );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n" );

	// Close file
	NLib_Fichier_NFichierTexte_Detruire( &file );

	// OK
	return NTRUE;
}
