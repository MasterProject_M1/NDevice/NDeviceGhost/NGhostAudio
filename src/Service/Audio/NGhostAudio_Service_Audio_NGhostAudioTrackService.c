#define NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSERVICE_INTERNE
#include "../../../include/NGhostAudio.h"

// ---------------------------------------------------------
// enum NGhostAudio::Service::Audio::NGhostAudioTrackService
// ---------------------------------------------------------

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostAudioTrackService NGhostAudio_Service_Audio_NGhostAudioTrackService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	NGhostAudioTrackService out = (NGhostAudioTrackService)0;

	// Buffer
	char *buffer;

	// Read service
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_AUDIO_TRACK_SERVICES;
	}

	// Look for service
	for( ; out < NGHOST_AUDIO_TRACK_SERVICES; out++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NGhostAudioTrackServiceName[ out ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return out;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_AUDIO_TRACK_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackService_GetName( NGhostAudioTrackService serviceType )
{
	return NGhostAudioTrackServiceName[ serviceType ];
}
