#define NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKSTATESERVICE_INTERNE
#include "../../../include/NGhostAudio.h"

// --------------------------------------------------------------
// enum NGhostAudio::Service::Audio::NGhostAudioTrackStateService
// --------------------------------------------------------------

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostAudioTrackStateService NGhostAudio_Service_Audio_NGhostAudioTrackStateService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	NGhostAudioTrackStateService out = (NGhostAudioTrackStateService)0;

	// Buffer
	char *buffer;

	// Read service
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOST_AUDIO_TRACK_STATE_SERVICES;
	}

	// Look for service
	for( ; out < NGHOST_AUDIO_TRACK_STATE_SERVICES; out++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NGhostAudioTrackStateServiceName[ out ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return out;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_AUDIO_TRACK_STATE_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetName( NGhostAudioTrackStateService serviceType )
{
	return NGhostAudioTrackStateServiceName[ serviceType ];
}

/**
 * Get full path
 *
 * @param serviceType
 * 		The service type
 *
 * @return the full path
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGhostAudioTrackStateService serviceType )
{
	return NGhostAudioTrackStateServiceFullPath[ serviceType ];
}
