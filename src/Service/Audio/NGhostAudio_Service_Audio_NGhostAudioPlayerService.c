#define NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOPLAYERSERVICE_INTERNE
#include "../../../include/NGhostAudio.h"

// ----------------------------------------------------------
// enum NGhostAudio::Service::Audio::NGhostAudioPlayerService
// ----------------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param ghostAudioPlayer
 * 		The protected ghost audio player
 * @param trackIndex
 * 		The requested track index
 *
 * @return the service or NGHOST_AUDIO_PLAYER_SERVICES
 */
NGhostAudioPlayerService NGhostAudio_Service_Audio_NGhostAudioPlayerService_FindService( const char *requestedElement,
	NU32 *cursor,
	__MUSTBEPROTECTED const NGhostAudioPlayer *ghostAudioPlayer,
	__OUTPUT NU32 *trackIndex )
{
	// Buffer
	char *buffer;

	// Output
	__OUTPUT NGhostAudioPlayerService out = (NGhostAudioPlayerService)0;

	// Read service
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NGHOST_AUDIO_PLAYER_SERVICES;
	}

	// Look for
	for( ; out < NGHOST_AUDIO_PLAYER_SERVICES; out++ )
		// Check service
		switch( out )
		{
			case NGHOST_AUDIO_PLAYER_SERVICE_TRACK:
				// Check length
				if( strlen( buffer ) <= 0 )
					break;

				// Is it a number?
				if( NLib_Chaine_EstUnNombreEntier( buffer,
					10 ) )
				{
					// Convert to number
					if( ( *trackIndex = (NU32)strtol( buffer,
						NULL,
						10 ) ) >= NGhostAudio_Audio_NGhostAudioPlayer_GetTrackCount( ghostAudioPlayer ) )
					{
						// Notify
						NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

						// Free
						NFREE( buffer );

						// Quit
						return NGHOST_AUDIO_PLAYER_SERVICES;
					}
				}
				// Is it a string?
				else
					if( ( *trackIndex = NGhostAudio_Audio_NGhostAudioPlayer_FindAudioObjectIndexWithName( ghostAudioPlayer,
						buffer ) ) == NERREUR )
					{
						// Notify
						NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

						// Free
						NFREE( buffer );

						// Quit
						return NGHOST_AUDIO_PLAYER_SERVICES;
					}

				// Free
				NFREE( buffer );

				// OK
				return out;

			default:
				if( NLib_Chaine_Comparer( buffer,
					NGhostAudioPlayerServiceName[ out ],
					NTRUE,
					0 ) )
				{
					// Free
					NFREE( buffer );

					// OK
					return out;
				}
				break;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOST_AUDIO_PLAYER_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostAudio_Service_Audio_NGhostAudioPlayerService_GetName( NGhostAudioPlayerService serviceType )
{
	return NGhostAudioPlayerServiceName[ serviceType ];
}
