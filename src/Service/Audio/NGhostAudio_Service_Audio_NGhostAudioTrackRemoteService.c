#define NGHOSTAUDIO_SERVICE_AUDIO_NGHOSTAUDIOTRACKREMOTESERVICE_INTERNE
#include "../../../include/NGhostAudio.h"

// ---------------------------------------------------------------
// enum NGhostAudio::Service::Audio::NGhostAudioTrackRemoteService
// ---------------------------------------------------------------

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NGhostAudioTrackRemoteService NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	NGhostAudioTrackRemoteService out = (NGhostAudioTrackRemoteService)0;

	// Buffer
	char *buffer;

	// Read service
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NGHOSTAUDIO_TRACK_REMOTE_SERVICES;
	}

	// Look for service
	for( ; out < NGHOSTAUDIO_TRACK_REMOTE_SERVICES; out++ )
		// Check
		if( NLib_Chaine_Comparer( buffer,
			NGhostAudioTrackRemoteServiceName[ out ],
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return out;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NGHOSTAUDIO_TRACK_REMOTE_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetName( NGhostAudioTrackRemoteService serviceType )
{
	return NGhostAudioTrackRemoteServiceName[ serviceType ];
}

/**
 * Get service full path
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service full path
 */
const char *NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGhostAudioTrackRemoteService serviceType )
{
	return NGhostAudioTrackRemoteServiceFullPath[ serviceType ];
}

