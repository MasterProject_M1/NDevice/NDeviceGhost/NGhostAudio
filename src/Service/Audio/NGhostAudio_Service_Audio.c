#include "../../../include/NGhostAudio.h"

// -------------------------------------
// namespace NGhostAudio::Service::Audio
// -------------------------------------

/**
 * Process REST request
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param requestType
 * 		The http request type
 * @param data
 * 		The user sent data
 * @param dataLength
 * 		The user sent data length
 * @param ghostAudio
 * 		The ghost audio instance
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostAudio_Service_Audio_ProcessRESTRequest( const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP requestType,
	const char *data,
	NU32 dataLength,
	void *ghostAudio,
	NU32 clientID )
{
	// Result
	__OUTPUT NReponseHTTP *response = NULL;

	// User data
	NParserOutputList *userData = NULL;

	// Ghost audio player
	NGhostAudioPlayer *ghostAudioPlayer;

	// Get audio player
	if( !( ghostAudioPlayer = (NGhostAudioPlayer*)NGhostAudio_NGhostAudio_GetAudioPlayer( ghostAudio ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NULL;
	}

	// Do we have user data?
	if( data != NULL
		&& dataLength > 0 )
		// Parse user data
		if( !( userData = NJson_Engine_Parser_Parse( data,
			dataLength ) ) )
		{
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Quit
			return NULL;
		}

	// Process request
	switch( requestType )
	{
		case NTYPE_REQUETE_HTTP_GET:
			response = NGhostAudio_Audio_NGhostAudioPlayer_ProcessGETRequest( ghostAudioPlayer,
				requestedElement,
				cursor,
				clientID );
			break;
		case NTYPE_REQUETE_HTTP_POST:
			// Process
			response = NGhostAudio_Audio_NGhostAudioPlayer_ProcessRESTPostRequest( ghostAudioPlayer,
				userData,
				clientID );
			break;
		case NTYPE_REQUETE_HTTP_PUT:
			response = NGhostAudio_Audio_NGhostAudioPlayer_ProcessRESTPUTRequest( ghostAudioPlayer,
				requestedElement,
				cursor,
				userData,
				clientID );
			break;
		case NTYPE_REQUETE_HTTP_DELETE:
			response = NGhostAudio_Audio_NGhostAudioPlayer_ProcessRESTDeleteRequest( ghostAudioPlayer,
				requestedElement,
				cursor,
				clientID );
			break;

		default:
			break;
	}

	// User data?
	if( userData != NULL )
		// Destroy user data
		NParser_Output_NParserOutputList_Destroy( &userData );

	// OK?
	return response;
}
