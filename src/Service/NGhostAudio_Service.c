#include "../../include/NGhostAudio.h"

// ------------------------------
// namespace NGhostAudio::Service
// ------------------------------

/**
 * Process an HTTP request (private)
 *
 * @param request
 * 		The http request
 * @param client
 * 		The requesting client
 *
 * @return the HTTP response
 */
__PRIVATE __ALLOC NReponseHTTP *NGhostAudio_Service_ProcessRequestInternal( const NRequeteHTTP *request,
	const NClientServeur *client )
{
	// HTTP Server
	const NHTTPServeur *httpServer;

	// HTTP response
	NReponseHTTP *response;

	// Service type
	NGhostCommonService serviceType;

	// Ghost audio
	NGhostAudio *ghostAudio;

	// Ghost common
	NGhostCommon *ghostCommon;

	// Cursor
	NU32 cursor = 0;

	// Authentication correct?
	NBOOL isAuthenticationCorrect;

	// Get server and client with configuration
	if( !( httpServer = (NHTTPServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ) )
		|| !( ghostAudio = (NGhostAudio*)NHTTP_Serveur_NHTTPServeur_ObtenirDataUtilisateur( httpServer ) )
		|| !( ghostCommon = NGhostAudio_NGhostAudio_GetGhostCommon( ghostAudio ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NULL;
	}

	// Process standard requests
	if( ( response = NGhostCommon_Service_ProcessBasicService( request,
		ghostCommon,
		&isAuthenticationCorrect,
		&serviceType,
		&cursor,
		client,
		"127.0.0.1" ) ) != NULL )
		return response;

	// Are we authenticated?
	if( !isAuthenticationCorrect )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_USER );

		// Quit
		return NULL;
	}
	
	// Analyze service
	switch( NGhostAudio_Service_NGhostAudioServiceType_FindService( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
		&cursor,
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( request ) ) )
	{
		case NGHOST_AUDIO_SERVICE_TYPE_AUDIO_GET:
		case NGHOST_AUDIO_SERVICE_TYPE_AUDIO_POST:
		case NGHOST_AUDIO_SERVICE_TYPE_AUDIO_PUT:
		case NGHOST_AUDIO_SERVICE_TYPE_AUDIO_DELETE:
			return ( response = NGhostAudio_Service_Audio_ProcessRESTRequest( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
				&cursor,
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( request ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirData( request ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirTailleData( request ),
				ghostAudio,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) != NULL ?
					response
					: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
						NHTTP_CODE_400_BAD_REQUEST,
						NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_PROTOCOL );

			// Quit
			return NULL;
	}
}

/**
 * Process an HTTP request
 *
 * @param request
 * 		The http request
 * @param client
 * 		The requesting client
 *
 * @return the HTTP response
 */
__ALLOC NReponseHTTP *NGhostAudio_Service_ProcessRequest( const NRequeteHTTP *request,
	const NClientServeur *client )
{
	// Response packet
	NReponseHTTP *response;

	// Process request
	if( !( response = NGhostAudio_Service_ProcessRequestInternal( request,
		client ) ) )
		return NGhostCommon_Service_CreateInternalErrorResponse( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

	// OK
	return response;
}
