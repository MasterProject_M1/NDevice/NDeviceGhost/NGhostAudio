#define NGHOSTAUDIO_SERVICE_NGHOSTAUDIOSERVICETYPE_INTERNE
#include "../../include/NGhostAudio.h"

// -------------------------------------------------
// enum NGhostAudio::Service::NGhostAudioServiceType
// -------------------------------------------------

/**
 * Get name API
 *
 * @param serviceType
 * 		The service type
 *
 * @return the api URI
 */
const char *NGhostAudio_Service_NGhostAudioServiceType_GetServiceURI( NGhostAudioServiceType serviceType )
{
	return NGhostAudioServiceTypeURI[ serviceType ];
}

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param httpRequestType
 * 		The http request type
 *
 * @return the service type
 */
NGhostAudioServiceType NGhostAudio_Service_NGhostAudioServiceType_FindService( const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP httpRequestType )
{
	// Iterator
	__OUTPUT NGhostAudioServiceType output = (NGhostAudioServiceType)0;

	// Look for
	for( ; output < NGHOST_AUDIO_SERVICE_TYPES; output++ )
		// Check if this is the requested service
		if( NLib_Chaine_EstChaineCommencePar( requestedElement,
			NGhostAudioServiceTypeURI[ output ] )
			&& httpRequestType == NGhostAudioServiceHTTPRequestType[ output ] )
		{
			// Save in cursor
			*cursor = (NU32)strlen( NGhostAudioServiceTypeURI[ output ] );

			// Found it!
			return output;
		}


	// Not found
	return NGHOST_AUDIO_SERVICE_TYPES;
}

