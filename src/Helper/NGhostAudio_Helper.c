#include "../../include/NGhostAudio.h"

// -----------------------------
// namespace NGhostAudio::Helper
// -----------------------------

/**
 * Build version
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Helper_BuildVersion( __OUTPUT char version[ 32 ] )
{
	// Cursor
	NU32 cursor = 0;

	// Clear
	memset( version,
		0,
		32 );

	// Build
	version[ cursor++ ] = VERSION_MAJOR_INIT;
	version[ cursor++ ] += '.';
	version[ cursor++ ] += VERSION_MINOR_INIT;
	version[ cursor++ ] += '-';
	version[ cursor++ ] += 'V';
	version[ cursor++ ] += '-';
	version[ cursor++ ] += BUILD_YEAR_CH0;
	version[ cursor++ ] += BUILD_YEAR_CH1;
	version[ cursor++ ] += BUILD_YEAR_CH2;
	version[ cursor++ ] += BUILD_YEAR_CH3;
	version[ cursor++ ] += '-';
	version[ cursor++ ] += BUILD_MONTH_CH0;
	version[ cursor++ ] += BUILD_MONTH_CH1;
	version[ cursor++ ] += '-';
	version[ cursor++ ] += BUILD_DAY_CH0;
	version[ cursor++ ] += BUILD_DAY_CH1;
	version[ cursor++ ] += 'T';
	version[ cursor++ ] += BUILD_HOUR_CH0;
	version[ cursor++ ] += BUILD_HOUR_CH1;
	version[ cursor++ ] += ':';
	version[ cursor++ ] += BUILD_MIN_CH0;
	version[ cursor++ ] += BUILD_MIN_CH1;
	version[ cursor++ ] += ':';
	version[ cursor++ ] += BUILD_SEC_CH0;
	version[ cursor++ ] += BUILD_SEC_CH1;
	version[ cursor ] += '\0';
}