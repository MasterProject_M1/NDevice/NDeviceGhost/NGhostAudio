#include "../../../include/NGhostAudio.h"

// -------------------------------------------------------------------
// struct NGhostAudio::Audio::Synchronization::NGhostAudioSynchronizer
// -------------------------------------------------------------------

/**
 * Build synchronizer
 *
 * @param synchList
 * 		The synchronization list
 * @param ghostAudioObject
 * 		The audio instance
 * @param authenticationManager
 * 		The authentication manager
 *
 * @return the synchronizer instance
 */
__ALLOC NGhostAudioSynchronizer *NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_Build( const NListe *synchList,
	void *ghostAudioObject,
	const NAuthenticationManager *authenticationManager )
{
	// Output
	__OUTPUT NGhostAudioSynchronizer *out;

	// Iterator
	NU32 i;

	// Hostname
	const char *hostname;

	// Track synch item
	NGhostAudioTrackSynchronizationState *state;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAudioSynchronizer ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_ghostAudioObject = ghostAudioObject;

	// Build list
	if( !( out->m_remote = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build synchronized items
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( synchList ); i++ )
		// Get hostname
		if( ( hostname = NLib_Memoire_NListe_ObtenirElementDepuisIndex( synchList,
			i ) ) != NULL )
		{
			// Build synch item
			if( !( state = NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_Build( hostname,
				ghostAudioObject,
				authenticationManager ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Destroy list
				NLib_Memoire_NListe_Detruire( &out->m_remote );

				// Quit
				return NULL;
			}

			// Add state to list
			if( !NLib_Memoire_NListe_Ajouter( out->m_remote,
				state ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

				// Destroy list
				NLib_Memoire_NListe_Detruire( &out->m_remote );

				// Quit
				return NULL;
			}
		}

	// OK
	return out;
}

/**
 * Destroy synchronizer
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_Destroy( NGhostAudioSynchronizer **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_remote );

	// Free
	NFREE( (*this) );
}

/**
 * Is synchronized?
 *
 * @param this
 * 		This instance
 *
 * @return if all the remote players are ready for simultaneous command
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_IsSynchronized( const NGhostAudioSynchronizer *this )
{
	// Iterator
	NU32 i = 0;

	// Element
	const NGhostAudioTrackSynchronizationState *element;

	// Check
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_remote ); i++ )
		// Get element
		if( ( element = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_remote,
			i ) ) != NULL )
			// Is it synchronized?
			if( !NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_IsReady( element ) )
				return NFALSE;

	// Everything is ready
	return NTRUE;
}

/**
 * Synchronized start
 *
 * @param this
 * 		This instance
 * @param position
 * 		The position in the audio (ms, NERREUR to ignore)
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizeStart( NGhostAudioSynchronizer *this,
	NU32 position )
{
	// Iterator
	NU32 i;

	// Audio
	NMusique *audio;

	// Remote
	NGhostAudioTrackSynchronizationState *remote;

	// Check if synch
	if( !NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_IsSynchronized( this ) )
		return NFALSE;

	// Start every player
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_remote ); i++ )
		// Get remote
		if( ( remote = (NGhostAudioTrackSynchronizationState*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_remote,
			i ) ) != NULL )
			NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetPause( remote,
				NFALSE,
				position );

	// Get audio
	if( ( audio = NGhostAudio_Audio_NGhostAudioObject_GetAudio( this->m_ghostAudioObject ) ) )
	{
		// Local delay to be near
		NLib_Temps_Attendre( NGHOST_AUDIO_SYNCHRONIZATION_DELAY_BEFORE_START );

		// Set position localy
		return NLib_Module_FModex_NMusique_Lire( audio );
	}

	// Failed
	return NFALSE;
}

/**
 * Synchronized stop
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizePause( NGhostAudioSynchronizer *this )
{
	// Iterator
	NU32 i;

	// Audio
	NMusique *audio;

	// Remote
	NGhostAudioTrackSynchronizationState *remote;

	// Check if synch
	if( !NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_IsSynchronized( this ) )
		return NFALSE;

	// Pause every player
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_remote ); i++ )
		// Get remote
		if( ( remote = (NGhostAudioTrackSynchronizationState*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_remote,
			i ) ) != NULL )
			NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetPause( remote,
				NTRUE,
				NERREUR );

	// Get audio
	if( ( audio = NGhostAudio_Audio_NGhostAudioObject_GetAudio( this->m_ghostAudioObject ) ) )
		// Set position localy
		return NLib_Module_FModex_NMusique_Pause( audio );

	// Fail
	return NFALSE;
}

/**
 * Synchronized volume set
 *
 * @param this
 * 		This instance
 * @param volume
 * 		The volume to be set
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizeVolumeSet( NGhostAudioSynchronizer *this,
	NU32 volume )
{
	// Iterator
	NU32 i;

	// Audio
	NMusique *audio;

	// Remote
	NGhostAudioTrackSynchronizationState *remote;

	// Check if synch
	if( !NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_IsSynchronized( this ) )
		return NFALSE;

	// Set volume for every player
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_remote ); i++ )
		// Get remote
		if( ( remote = (NGhostAudioTrackSynchronizationState*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_remote,
			i ) ) != NULL )
			NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetVolume( remote,
				volume );

	// Get audio
	if( ( audio = NGhostAudio_Audio_NGhostAudioObject_GetAudio( this->m_ghostAudioObject ) ) )
		// Set position localy
		return NLib_Module_FModex_NMusique_DefinirVolume( audio,
			volume );

	// Fail
	return NFALSE;
}

/**
 * Synchronized position set
 *
 * @param this
 * 		This instance
 * @param position
 * 		The position to be set
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizePositionSet( NGhostAudioSynchronizer *this,
	NU32 position )
{
	// Iterator
	NU32 i;

	// Audio
	NMusique *audio;

	// Remote
	NGhostAudioTrackSynchronizationState *remote;

	// Check if synch
	if( !NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_IsSynchronized( this ) )
		return NFALSE;

	// Set position for every player
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_remote ); i++ )
		// Get remote
		if( ( remote = (NGhostAudioTrackSynchronizationState*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_remote,
			i ) ) != NULL )
			NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetPosition( remote,
				position );

	// Get audio
	if( ( audio = NGhostAudio_Audio_NGhostAudioObject_GetAudio( this->m_ghostAudioObject ) ) )
		// Set position localy
		return NLib_Module_FModex_NMusique_DefinirPosition( audio,
			position );

	// Fail
	return NFALSE;
}
