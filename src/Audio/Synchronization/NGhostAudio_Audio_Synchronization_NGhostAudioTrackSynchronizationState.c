#include "../../../include/NGhostAudio.h"

// --------------------------------------------------------------------------------
// struct NGhostAudio::Audio::Synchronization::NGhostAudioTrackSynchronizationState
// --------------------------------------------------------------------------------

/**
 * Receive callback
 *
 * @param packet
 * 		The received packet
 * @param client
 * 		The client
 *
 * @return if the operation succeeded
 */
__PRIVATE __CALLBACK NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_ReceiveCallback( const NPacket *packet,
	const NClient *client )
{
	// HTTP client
	NClientHTTP *httpClient;

	// Ghost audio track synch
	NGhostAudioTrackSynchronizationState *this;

	// HTTP response
	NReponseHTTP *response;

	// Secure data
	char *secureData;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Parser output
	const NParserOutput *parserOutput;

	// Get synchronizer instance
	if( !( httpClient = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) )
		|| !( this = NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( httpClient ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Allocate secure data
	if( !( secureData = calloc( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) + 1,
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NFALSE;
	}

	// Copy data
	memcpy( secureData,
		NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) );

	// Parse response
	if( !( response = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire2( secureData ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( secureData );

		// Quit
		return NFALSE;
	}

	// Free
	NFREE( secureData );

	// Get data
	if( !( secureData = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( response ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Destroy response
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &response );

		// Quit
		return NFALSE;
	}

	// Destroy http response
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &response );

	// Parser json
	if( !( parserOutputList = NJson_Engine_Parser_Parse( secureData,
		(NU32)strlen( secureData ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( secureData );

		// Quit
		return NFALSE;
	}

	// Free
	NFREE( secureData );

	// We have successfully received the confirmation of track add
	this->m_isWaitingTrackAdd = NFALSE;

	// Look for ready state
	if( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_READY ),
		NFALSE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_BOOLEAN )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Destroy parser output list
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Check readiness state
	if( *(NBOOL *)NParser_Output_NParserOutput_GetValue( parserOutput ) == NTRUE )
		this->m_isReady = NTRUE;

	// Destroy parser output list
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// OK
	return NTRUE;
}

/**
 * Send audio request to remote
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SendAudioRequest( const NGhostAudioTrackSynchronizationState *this )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Result
	__OUTPUT NBOOL result;

	// Json
	char *json;

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add track properties
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackService_GetName( NGHOST_AUDIO_TRACK_SERVICE_NAME ),
		NGhostAudio_Audio_NGhostAudioObject_GetName( this->m_ghostAudioObject ) );
	NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackService_GetName( NGHOST_AUDIO_TRACK_SERVICE_IS_LOOP ),
		NGhostAudio_Audio_NGhostAudioObject_IsLoop( this->m_ghostAudioObject ) );

	// Add track remote properties
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGHOSTAUDIO_TRACK_REMOTE_SERVICE_FILE_PATH ),
		NGhostAudio_Audio_NGhostAudioObject_GetRemoteFilePath( this->m_ghostAudioObject ) );
	NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGHOSTAUDIO_TRACK_REMOTE_SERVICE_HOSTNAME ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( this->m_audioSourceAuthenticationEntry ) );
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGHOSTAUDIO_TRACK_REMOTE_SERVICE_PORT ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( this->m_audioSourceAuthenticationEntry ) );

	// Add track state properties
	NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED ),
		NTRUE );
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGHOST_AUDIO_TRACK_STATE_SERVICE_POSITION ),
		NGhostAudio_Audio_NGhostAudioObject_GetPosition( this->m_ghostAudioObject ) );
	NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGHOST_AUDIO_TRACK_STATE_SERVICE_VOLUME ),
		NGhostAudio_Audio_NGhostAudioObject_GetVolume( this->m_ghostAudioObject ) );

	// Build json
	if( !( json = NJson_Engine_Builder_Build( parserOutputList ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserOutputList );

		// Quit
		return NFALSE;
	}

	// Destroy
	NParser_Output_NParserOutputList_Destroy( &parserOutputList );

	// Send packet
	result = NHTTP_Client_NClientHTTP_EnvoyerRequete3( this->m_client,
		NTYPE_REQUETE_HTTP_POST,
		NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE,
		json,
		(NU32)strlen( json ),
		NGHOST_AUDIO_SYNCHRONIZATION_CONNECTION_TIMEOUT,
		NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_audioPlayerAuthenticationEntry ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this->m_audioPlayerAuthenticationEntry ) );

	// Free
	NFREE( json );

	// OK
	return result;
}

/**
 * Synchronization thread
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
__PRIVATE __THREAD NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SynchronizationThread( NGhostAudioTrackSynchronizationState *this )
{
	// URL
	char url[ 256 ];

	// Retry count for initial POST request
	NU32 postRetryCount = 0;

	// We are now waiting for the track
	this->m_isWaitingTrackAdd = NTRUE;

	// Send POST request
	do
	{
		// Send audio load request
		if( !NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SendAudioRequest( this ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Quit
			return this->m_isSynchronizationThreadRunning = NFALSE;
		}

		// Increase retry count
		postRetryCount++;

		// Wait
		NLib_Temps_Attendre( NGHOST_AUDIO_SYNCHRONIZATION_RETRY_DELAY );
	} while( this->m_isSynchronizationThreadRunning
	 	&& postRetryCount < NGHOST_AUDIO_SYNCHRONIZATION_MAXIMUM_INITIAL_POST_REQUEST
		&& ( NHTTP_Client_NClientHTTP_EstPacketAttente( this->m_client )
			|| this->m_isWaitingTrackAdd ) );

	// Check if remote side added track
	if( this->m_isWaitingTrackAdd )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quit
		return this->m_isSynchronizationThreadRunning = NFALSE;
	}

	// Build url
	snprintf( url,
		256,
		"%s%s",
		NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE,
		this->m_trackName );

	while( this->m_isSynchronizationThreadRunning
		&& !this->m_isReady )
	{
		// Send request
		NHTTP_Client_NClientHTTP_EnvoyerRequete3( this->m_client,
			NTYPE_REQUETE_HTTP_GET,
			url,
			NULL,
			0,
			NGHOST_AUDIO_SYNCHRONIZATION_CONNECTION_TIMEOUT,
			NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_audioPlayerAuthenticationEntry ),
			NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this->m_audioPlayerAuthenticationEntry ) );

		// Wait
		NLib_Temps_Attendre( NGHOST_AUDIO_SYNCHRONIZATION_RETRY_DELAY );
	}

	// OK
	return NTRUE;
}

/**
 * Build remote audio state
 *
 * @param hostname
 * 		The remote hostname to synchronize with
 * @param ghostAudioObject
 * 		The ghost audio object
 * @param authenticationManager
 * 		The authentication manager
 * @param audioSourceAuthenticationEntry
 * 		The audio source authentication entry
 *
 * @return the built instance
 */
__ALLOC NGhostAudioTrackSynchronizationState *NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_Build( const char *hostname,
	void *ghostAudioObject,
	const NAuthenticationManager *authenticationManager )
{
	// Output
	__OUTPUT NGhostAudioTrackSynchronizationState *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAudioTrackSynchronizationState ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_ghostAudioObject = ghostAudioObject;
	out->m_audioSourceAuthenticationEntry = NGhostAudio_Audio_NGhostAudioObject_GetAuthenticationEntry( ghostAudioObject );

	// Get authentication entry
	if( !( out->m_audioPlayerAuthenticationEntry = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForHostnameAndPort( authenticationManager,
		hostname,
		NDEVICE_COMMON_TYPE_GHOST_AUDIO_LISTENING_PORT ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Duplicate track name
	if( !( out->m_trackName = NLib_Chaine_Dupliquer( NGhostAudio_Audio_NGhostAudioObject_GetName( ghostAudioObject ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build http client
	if( !( out->m_client = NHTTP_Client_NClientHTTP_Construire2( hostname,
		NDEVICE_COMMON_TYPE_GHOST_AUDIO_LISTENING_PORT,
		out,
		NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_ReceiveCallback ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_trackName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Synchronization thread
	out->m_isSynchronizationThreadRunning = NTRUE;
	if( !( out->m_synchronizationThread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SynchronizationThread,
		out,
		&out->m_isSynchronizationThreadRunning ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy HTTP client
		NHTTP_Client_NClientHTTP_Detruire( &out->m_client );

		// Free
		NFREE( out->m_trackName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy instance
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_Destroy( NGhostAudioTrackSynchronizationState **this )
{
	// Destroy synchronization thread
	NLib_Thread_NThread_Detruire( &(*this)->m_synchronizationThread );

	// Destroy HTTP client
	NHTTP_Client_NClientHTTP_Detruire( &(*this)->m_client );

	// Free
	NFREE( (*this)->m_trackName );
	NFREE( (*this) );
}

/**
 * Is ready?
 *
 * @param this
 * 		This instance
 *
 * @return if the remote client is ready
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_IsReady( const NGhostAudioTrackSynchronizationState *this )
{
	return this->m_isReady;
}

/**
 * Internal state broadcast
 *
 * @param this
 * 		This instance
 * @param isPaused
 * 		Is paused? (Ignored if NERREUR)
 * @param volume
 * 		The volume (Ignored if NERREUR)
 * @param position
 * 		The position (Ignorer if NERREUR)
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_BroadcastUpdate( NGhostAudioTrackSynchronizationState *this,
	NBOOL isPaused,
	NU32 volume,
	NU32 position )
{
	// URL
	char url[ 256 ];

	// Data
	char data[ 256 ];

	// Paused
	char isPausedData[ 256 ];

	// Volume data
	char volumeData[ 256 ];

	// Position data
	char positionData[ 256 ];

	// Build url
	snprintf( url,
		256,
		"%s%s/%s",
		NDEVICE_COMMON_TYPE_GHOST_AUDIO_AUDIO_SERVICE,
		this->m_trackName,
		NGHOSTAUDIO_SERVICE_AUDIO_STATE_ROOT );

	// Build data
	snprintf( isPausedData,
		256,
		"\"%s\": %s,",
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetName( NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED ),
		isPaused ?
			NTRUE_KEYWORD
			: NFALSE_KEYWORD );
	snprintf( volumeData,
		256,
		"\"%s\": %d,",
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetName( NGHOST_AUDIO_TRACK_STATE_SERVICE_VOLUME ),
		volume );
	snprintf( positionData,
		256,
		"\"%s\": %d",
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetName( NGHOST_AUDIO_TRACK_STATE_SERVICE_POSITION ),
		position );

	// Build final data
	snprintf( data,
		256,
		"{ %s %s %s }",
		isPaused != NERREUR ?
			isPausedData
			: "",
		volume != NERREUR ?
			volumeData
			: "",
		position != NERREUR ?
			positionData
			: "" );

	// Send request
	return NHTTP_Client_NClientHTTP_EnvoyerRequete3( this->m_client,
		NTYPE_REQUETE_HTTP_PUT,
		url,
		data,
		(NU32)strlen( data ),
		NGHOST_AUDIO_SYNCHRONIZATION_RETRY_DELAY,
		NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_audioPlayerAuthenticationEntry ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this->m_audioPlayerAuthenticationEntry ) );
}

/**
 * Set is paused
 *
 * @param this
 * 		This instance
 * @param isPaused
 * 		Paused?
 * @param position
 * 		The position to set at startup (NERREUR to ignore)
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetPause( NGhostAudioTrackSynchronizationState *this,
	NBOOL isPaused,
	NU32 position )
{
	return NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_BroadcastUpdate( this,
		isPaused,
		NERREUR,
		position );
}

/**
 * Set volume
 *
 * @param this
 * 		This instance
 * @param volume
 * 		The volume (0-100)
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetVolume( NGhostAudioTrackSynchronizationState *this,
	NU32 volume )
{
	return NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_BroadcastUpdate( this,
		NERREUR,
		volume,
		NERREUR );
}

/**
 * Set position
 *
 * @param this
 * 		This instance
 * @param position
 * 		The position
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_SetPosition( NGhostAudioTrackSynchronizationState *this,
	NU32 position )
{
	return NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_BroadcastUpdate( this,
		NERREUR,
		NERREUR,
		position );
}

/**
 * Is error
 *
 * @param this
 * 		This instance
 *
 * @return if there has been an error
 */
NBOOL NGhostAudio_Audio_Synchronization_NGhostAudioTrackSynchronizationState_IsError( const NGhostAudioTrackSynchronizationState *this )
{
	return (NBOOL)( this->m_isWaitingTrackAdd
		&& !this->m_isSynchronizationThreadRunning );
}
