#include "../../include/NGhostAudio.h"

// --------------------------------------------
// struct NGhostAudio::Audio::NGhostAudioPlayer
// --------------------------------------------

/**
 * Player cleaning thread
 *
 * @param this
 * 		This instance
 *
 * @return NTRUE
 */
__PRIVATE __THREAD __WILLLOCK __WILLUNLOCK NBOOL NGhostAudio_Audio_NGhostAudioPlayer_ThreadCleaning( NGhostAudioPlayer *this )
{
	// Iterator
	NU32 i;

	// Audio object
	const NGhostAudioObject *ghostAudioObject;

	// While cleaner is operating
	while( this->m_isCleaningThreadRunning )
	{
		// Lock
		NLib_Memoire_NListe_ActiverProtection( this->m_object );

		// Iterate objects
		for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_object ); i++ )
			// Get object
			if( ( ghostAudioObject = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_object,
				i ) ) != NULL )
				// Check
				if( NGhostAudio_Audio_NGhostAudioObject_IsLoadingDone( ghostAudioObject )
					&& NGhostAudio_Audio_NGhostAudioObject_IsError( ghostAudioObject ) )
				{
					// Remove from list
					NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_object,
						i );

					// Decrement i
					if( i > 0 )
						i--;
				}

		// Unlock
		NLib_Memoire_NListe_DesactiverProtection( this->m_object );

		// Wait for few ms
		NLib_Temps_Attendre( NGHOSTAUDIO_AUDIO_DELAY_BETWEEN_CLEANING_STEP );
	}

	// OK
	return NTRUE;
}

/**
 * Build ghost audio player
 *
 * @param authenticationManager
 * 		The authentication manager
 *
 * @return the ghost audio player
 */
__ALLOC NGhostAudioPlayer *NGhostAudio_Audio_NGhostAudioPlayer_Build( NAuthenticationManager *authenticationManager )
{
	// Output
	__OUTPUT NGhostAudioPlayer *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAudioPlayer ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_authenticationManager = authenticationManager;

	// Build list
	if( !( out->m_object = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NGhostAudio_Audio_NGhostAudioObject_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build cleaning thread
	out->m_isCleaningThreadRunning = NTRUE;
	if( !( out->m_cleaningThread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl *)( void* ))NGhostAudio_Audio_NGhostAudioPlayer_ThreadCleaning,
		out,
		&out->m_isCleaningThreadRunning ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy audio object list
		NLib_Memoire_NListe_Detruire( &out->m_object );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy audio player
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioPlayer_Destroy( NGhostAudioPlayer **this )
{
	// Destroy thread
	NLib_Thread_NThread_Detruire( &(*this)->m_cleaningThread );

	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_object );

	// Free
	NFREE( (*this) );
}

/**
 * Add a song
 *
 * @param this
 * 		This instance
 * @param trackInfo
 * 		The track informations
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__WILLLOCK __WILLUNLOCK __ALLOC NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_AddAudioObject( NGhostAudioPlayer *this,
	const NGhostAudioTrackInfo *trackInfo,
	NU32 clientID )
{
	// Audio object
	NGhostAudioObject *audioObject;

	// Result
	NBOOL result;

	// Ghost audio object
	const NGhostAudioObject *ghostAudioObject;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_object );

	// Check if already exists
	if( ( ghostAudioObject = NGhostAudio_Audio_NGhostAudioPlayer_FindAudioObjectWithName( this,
		trackInfo->m_trackName ) ) != NULL )
	{
		// Build parser output list
		if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
		{
			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_object );

			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Clean track details
			NGhostAudio_Audio_NGhostAudioTrackInfo_Clean( (NGhostAudioTrackInfo*)trackInfo );

			// Quit
			return NULL;
		}

		// Output track info
		NGhostAudio_Audio_NGhostAudioObject_BuildTrackParserOutputList( ghostAudioObject,
			parserOutputList,
			"" );

		// Unlock
		NLib_Memoire_NListe_DesactiverProtection( this->m_object );

		// Clean track details
		NGhostAudio_Audio_NGhostAudioTrackInfo_Clean( (NGhostAudioTrackInfo*)trackInfo );

		// Build packet
		return NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTP_CODE_200_OK,
			parserOutputList,
			clientID );
	}

	// Build audio object
	if( !( audioObject = NGhostAudio_Audio_NGhostAudioObject_Build( (NGhostAudioTrackInfo*)trackInfo,
		this->m_authenticationManager ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Unlock
		NLib_Memoire_NListe_DesactiverProtection( this->m_object );

		// Quit
		return NULL;
	}

	// Add
	result = NLib_Memoire_NListe_Ajouter( this->m_object,
		audioObject );

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_object );

	// OK?
	return result ?
		NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
			NHTTP_CODE_200_OK,
			clientID )
		: NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_INTERNAL_ERROR_MESSAGE,
			NHTTP_CODE_500_INTERNAL_SERVER_ERROR,
			clientID ) ;
}

/**
 * Find audio object index with name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The audio object name
 *
 * @return the audio object index or NERREUR
 */
__MUSTBEPROTECTED NU32 NGhostAudio_Audio_NGhostAudioPlayer_FindAudioObjectIndexWithName( const NGhostAudioPlayer *this,
	const char *name )
{
	// Iterator
	NU32 i = 0;

	// Object
	const NGhostAudioObject *object;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_object ); i++ )
		// Get object
		if( ( object = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_object,
			i ) ) != NULL )
			// Check name
			if( NLib_Chaine_Comparer( name,
				NGhostAudio_Audio_NGhostAudioObject_GetName( object ),
				NFALSE,
				0 ) )
				// OK
				return i;

	// Not found
	return NERREUR;
}

/**
 * Find audio object with name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The audio object name
 *
 * @return the audio object instance or NULL
 */
__MUSTBEPROTECTED const NGhostAudioObject *NGhostAudio_Audio_NGhostAudioPlayer_FindAudioObjectWithName( const NGhostAudioPlayer *this,
	const char *name )
{
	// Index
	NU32 index;

	// Look for object
	if( ( index = NGhostAudio_Audio_NGhostAudioPlayer_FindAudioObjectIndexWithName( this,
		name ) ) == NERREUR )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Found it
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_object,
		index );
}

/**
 * Get track count
 *
 * @param this
 * 		This instance
 *
 * @return the track count
 */
__MUSTBEPROTECTED NU32 NGhostAudio_Audio_NGhostAudioPlayer_GetTrackCount( const NGhostAudioPlayer *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_object );
}

/**
 * Request track suppression by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The track index
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostAudio_Audio_NGhostAudioPlayer_RequestTrackSuppression( NGhostAudioPlayer *this,
	NU32 index )
{
	// Audio object
	NGhostAudioObject *ghostAudioObject;

	// Get instance
	if( !( ghostAudioObject = (NGhostAudioObject*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_object,
		index ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Check if already requested
	if( NGhostAudio_Audio_NGhostAudioObject_IsError( ghostAudioObject ) )
		return NFALSE;

	// Request
	NGhostAudio_Audio_NGhostAudioObject_RequestSuppression( ghostAudioObject );

	// OK
	return NTRUE;
}

/**
 * Request suppression of all tracks
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostAudio_Audio_NGhostAudioPlayer_RequestAllTrackSuppression( NGhostAudioPlayer *this )
{
	// Iterator
	NU32 i = 0;

	// Delete all tracks
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_object ); i++ )
		// Request suppression
		NGhostAudio_Audio_NGhostAudioPlayer_RequestTrackSuppression( this,
			i );

	// OK
	return NTRUE;
}

/**
 * Process POST request
 *
 * @param this
 * 		This instance
 * @param userData
 * 		The user data
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_ProcessRESTPostRequest( NGhostAudioPlayer *this,
	NParserOutputList *userData,
	NU32 clientID )
{
	// Track info
	NGhostAudioTrackInfo trackInfo;

	// Check user data
	if( !userData )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Extract data
	if( !NGhostAudio_Audio_NGhostAudioPlayer_ExtractUserTrackInfo( userData,
		&trackInfo ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NULL;
	}

	// Check data
	if( !trackInfo.m_trackName
		|| !trackInfo.m_hostname
		|| trackInfo.m_port == NERREUR
		|| !trackInfo.m_filePath
		|| !trackInfo.m_synchList )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Clean
		NGhostAudio_Audio_NGhostAudioTrackInfo_Clean( &trackInfo );

		// Quit
		return NULL;
	}

	// Add audio object
	return NGhostAudio_Audio_NGhostAudioPlayer_AddAudioObject( this,
		&trackInfo,
		clientID );
}

/**
 * Process PUT request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param userData
 * 		The user data
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC __WILLLOCK __WILLUNLOCK NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_ProcessRESTPUTRequest( NGhostAudioPlayer *this,
	const char *requestedElement,
	NU32 *cursor,
	const NParserOutputList *userData,
	NU32 clientID )
{
	// Track index
	NU32 trackIndex;

	// Audio object
	NGhostAudioObject *audioObject;

	// Check user data
	if( !userData )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_object );

	// Process
	switch( NGhostAudio_Service_Audio_NGhostAudioPlayerService_FindService( requestedElement,
		cursor,
		this,
		&trackIndex ) )
	{
		case NGHOST_AUDIO_PLAYER_SERVICE_TRACK:
			// Get audio object
			if( !( audioObject = (NGhostAudioObject*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_object,
				trackIndex ) ) )
			{
				// Unlock
				NLib_Memoire_NListe_DesactiverProtection( this->m_object );

				// Quit
				return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_INTERNAL_ERROR_MESSAGE,
					NHTTP_CODE_500_INTERNAL_SERVER_ERROR,
					clientID );
			}

			// Process request
			if( !NGhostAudio_Audio_NGhostAudioObject_ProcessTrackPUTRequest( audioObject,
				requestedElement,
				cursor,
				userData ) )
			{
				// Unlock
				NLib_Memoire_NListe_DesactiverProtection( this->m_object );

				// Quit
				return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
					NHTTP_CODE_400_BAD_REQUEST,
					clientID );
			}
			break;

		default:
			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_object );

			// Quit
			return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
				NHTTP_CODE_400_BAD_REQUEST,
				clientID );
	}

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_object );

	// OK
	return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
		NHTTP_CODE_200_OK,
		clientID );
}

/**
 * Build parser output list internal (private)
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 * @param root
 * 		The root
 * @param trackIndex
 * 		The track index
 *
 * @return if operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NGhostAudio_Audio_NGhostAudioPlayer_BuildParserOutputListInternal( NGhostAudioPlayer *this,
	NParserOutputList *parserOutputList,
	NGhostAudioPlayerService serviceType,
	const char *root,
	NU32 trackIndex )
{
	// Key root
	char keyRoot[ 256 ];

	// Audio object
	NGhostAudioObject *audioObject = NULL;

	// Init
	switch( serviceType )
	{
		default:
			snprintf( keyRoot,
				256,
				"%s%s%s",
				root,
				strlen( root ) > 0 ?
					"."
					: "",
				NGhostAudio_Service_Audio_NGhostAudioPlayerService_GetName( serviceType ) );
			break;

		case NGHOST_AUDIO_PLAYER_SERVICE_TRACK:
			// Get audio object
			if( !( audioObject = (NGhostAudioObject*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_object,
				trackIndex ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

				// Quit
				return NFALSE;
			}

			// Build key
			snprintf( keyRoot,
				256,
				"%s%s%s",
				root,
				strlen( root ) > 0 ?
					"."
					: "",
				NGhostAudio_Audio_NGhostAudioObject_GetName( audioObject ) );
			break;
	}

	// Add data
	switch( serviceType )
	{
		default:
			return NFALSE;

		case NGHOST_AUDIO_PLAYER_SERVICE_TRACK_COUNT:
			NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				keyRoot,
				NLib_Memoire_NListe_ObtenirNombre( this->m_object ) );
			break;

		case NGHOST_AUDIO_PLAYER_SERVICE_TRACK:
			NGhostAudio_Audio_NGhostAudioObject_BuildTrackParserOutputList( audioObject,
				parserOutputList,
				keyRoot );
			break;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NGhostAudio_Audio_NGhostAudioPlayer_BuildParserOutputList( NGhostAudioPlayer *this,
	__OUTPUT NParserOutputList *parserOutputList )
{
	// Iterator
	NGhostAudioPlayerService serviceType = (NGhostAudioPlayerService)0;

	// Iterator
	NU32 i = 0;

	// Iterate
	for( ; serviceType < NGHOST_AUDIO_PLAYER_SERVICES; serviceType++ )
		switch( serviceType )
		{
			case NGHOST_AUDIO_PLAYER_SERVICE_TRACK:
				// Iterate tracks
				for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_object ); i++ )
					NGhostAudio_Audio_NGhostAudioPlayer_BuildParserOutputListInternal( this,
						parserOutputList,
						serviceType,
						"",
						i );
				break;

			default:
				NGhostAudio_Audio_NGhostAudioPlayer_BuildParserOutputListInternal( this,
					parserOutputList,
					serviceType,
					"",
					0 );
				break;
		}

	// OK
	return NTRUE;
}

/**
 * Process GET request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param clientID
 * 		The client ID
 *
 * @return the http response
 */
__ALLOC __WILLLOCK __WILLUNLOCK NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_ProcessGETRequest( NGhostAudioPlayer *this,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID )
{
	// Parser output list
	NParserOutputList *parserOutputList;

	// Audio object
	const NGhostAudioObject *audioObject;

	// Service type
	NGhostAudioPlayerService serviceType;

	// Track index
	NU32 trackIndex;

	// Build parser output list
	if( !( parserOutputList = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_object );

	// Find requested service
	switch( ( serviceType = NGhostAudio_Service_Audio_NGhostAudioPlayerService_FindService( requestedElement,
		cursor,
		this,
		&trackIndex ) ) )
	{
		case NGHOST_AUDIO_PLAYER_SERVICE_ROOT:
			NGhostAudio_Audio_NGhostAudioPlayer_BuildParserOutputList( this,
				parserOutputList );
			break;

		case NGHOST_AUDIO_PLAYER_SERVICE_TRACK:
			// Get track
			if( ( audioObject = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_object,
				trackIndex ) ) != NULL )
				// Process
				if( !NGhostAudio_Audio_NGhostAudioObject_ProcessTrackGETRequest( audioObject,
					parserOutputList,
					requestedElement,
					cursor ) )
					// Notify
					NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );
			break;

		default:
			NGhostAudio_Audio_NGhostAudioPlayer_BuildParserOutputListInternal( this,
				parserOutputList,
				serviceType,
				"",
				trackIndex );
			break;
	}

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_object );

	// OK
	return NDeviceCommon_Service_GenerateJsonHTTPResponseWithParserOutputList( NHTTP_CODE_200_OK,
		parserOutputList,
		clientID );
}

/**
 * Process DELETE request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		Cursor in requested element
 * @param clientID
 * 		The client ID
 *
 * @return the response
 */
__ALLOC NReponseHTTP *NGhostAudio_Audio_NGhostAudioPlayer_ProcessRESTDeleteRequest( NGhostAudioPlayer *this,
	const char *requestedElement,
	NU32 *cursor,
	NU32 clientID )
{
	// Track index
	NU32 trackIndex;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_object );

	// Find requested service
	switch( NGhostAudio_Service_Audio_NGhostAudioPlayerService_FindService( requestedElement,
		cursor,
		this,
		&trackIndex ) )
	{
		case NGHOST_AUDIO_PLAYER_SERVICE_TRACK:
			// Request suppression
			if( !NGhostAudio_Audio_NGhostAudioPlayer_RequestTrackSuppression( this,
				trackIndex ) )
			{
				// Unlock
				NLib_Memoire_NListe_DesactiverProtection( this->m_object );

				// Bad request
				return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
					NHTTP_CODE_400_BAD_REQUEST,
					clientID );
			}
			break;

		default:
			// Check if track present
			if( NLib_Memoire_NListe_ObtenirNombre( this->m_object ) <= 0 )
			{
				// Unlock
				NLib_Memoire_NListe_DesactiverProtection( this->m_object );

				// Bad request
				return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
					NHTTP_CODE_400_BAD_REQUEST,
					clientID );
			}
			else
				NGhostAudio_Audio_NGhostAudioPlayer_RequestAllTrackSuppression( this );
	}

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_object );

	// OK
	return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_OK_MESSAGE,
		NHTTP_CODE_200_OK,
		clientID );
}

