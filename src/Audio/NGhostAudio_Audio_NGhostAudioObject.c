#include "../../include/NGhostAudio.h"

// --------------------------------------------
// struct NGhostAudio::Audio::NGhostAudioObject
// --------------------------------------------

/**
 * Build ghost audio object instance
 *
 * @param trackInfo
 * 		The track informations
 * @param authenticationManager
 * 		The authentication manager
 *
 * @return the audio object instance
 */
__ALLOC NGhostAudioObject *NGhostAudio_Audio_NGhostAudioObject_Build( __WILLBEOWNED NGhostAudioTrackInfo *trackInfo,
	const NAuthenticationManager *authenticationManager )
{
	// Output
	__OUTPUT NGhostAudioObject *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAudioObject ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Clear
		NGhostAudio_Audio_NGhostAudioTrackInfo_Clean( trackInfo );

		// Quit
		return NULL;
	}

	// Save
	out->m_volume = trackInfo->m_volume;
	out->m_isMustStart = trackInfo->m_isMustStart;
	out->m_isLoop = trackInfo->m_isLoop;
	out->m_synchronizedList = trackInfo->m_synchList;
	out->m_initialPosition = trackInfo->m_position;

	// Build remote details
	if( !( out->m_remote = NGhostAudio_Audio_NGhostAudioRemoteAudio_Build( trackInfo->m_hostname,
		trackInfo->m_port,
		authenticationManager,
		trackInfo->m_filePath,
		trackInfo->m_trackName,
		out ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy list
		NLib_Memoire_NListe_Detruire( &out->m_synchronizedList );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build synchronizer
	if( NLib_Memoire_NListe_ObtenirNombre( out->m_synchronizedList ) > 0 )
		if( !NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildSynchronizer( out->m_remote,
			authenticationManager ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Destroy remote
			NGhostAudio_Audio_NGhostAudioRemoteAudio_Destroy( &out->m_remote );

			// Destroy list
			NLib_Memoire_NListe_Detruire( &out->m_synchronizedList );

			// Free
			NFREE( out );

			// Quit
			return NULL;
		}

	// OK
	return out;
}

/**
 * Destroy audio instance
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioObject_Destroy( NGhostAudioObject **this )
{
	// Destroy remote details
	NGhostAudio_Audio_NGhostAudioRemoteAudio_Destroy( &(*this)->m_remote );

	// Destroy audio
	if( (*this)->m_audio != NULL )
		NLib_Module_FModex_NMusique_Detruire( &(*this)->m_audio );

	// Synchronized list?
	if( (*this)->m_synchronizedList != NULL )
		// Destroy synchronized list
		NLib_Memoire_NListe_Detruire( &(*this)->m_synchronizedList );

	// Free
	NFREE( (*this) );
}

/**
 * Get name
 *
 * @param this
 * 		This instance
 *
 * @return the name
 */
const char *NGhostAudio_Audio_NGhostAudioObject_GetName( const NGhostAudioObject *this )
{
	return NGhostAudio_Audio_NGhostAudioRemoteAudio_GetTrackName( this->m_remote );
}

/**
 * Is paused?
 *
 * @param this
 * 		This instance
 *
 * @return if the audio is paused
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_IsPaused( const NGhostAudioObject *this )
{
	if( this->m_audio != NULL )
		return NLib_Module_FModex_NMusique_EstPause( this->m_audio );
	else
		return NTRUE;
}

/**
 * Is there an error?
 *
 * @param this
 * 		This instance
 *
 * @return if there is an error
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_IsError( const NGhostAudioObject *this )
{
	return NGhostAudio_Audio_NGhostAudioRemoteAudio_IsError( this->m_remote );
}

/**
 * Is loading done?
 *
 * @param this
 * 		This instance
 *
 * @return if the loading is done
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_IsLoadingDone( const NGhostAudioObject *this )
{
	return NGhostAudio_Audio_NGhostAudioRemoteAudio_IsLoadingDone( this->m_remote );
}

/**
 * Is loop ?
 *
 * @param this
 * 		This instance
 *
 * @return if loop
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_IsLoop( const NGhostAudioObject *this )
{
	return this->m_isLoop;
}

/**
 * Get current position (ms) in audio
 *
 * @param this
 * 		This instance
 *
 * @return the audio position (ms)
 */
NU32 NGhostAudio_Audio_NGhostAudioObject_GetPosition( const NGhostAudioObject *this )
{
	if( this->m_audio != NULL )
		return NLib_Module_FModex_NMusique_ObtenirPosition( this->m_audio );
	else
		return this->m_initialPosition;
}

/**
 * Get volume
 *
 * @param this
 * 		This instance
 *
 * @return the volume (0-100)
 */
NU32 NGhostAudio_Audio_NGhostAudioObject_GetVolume( const NGhostAudioObject *this )
{
	return this->m_volume;
}

/**
 * Request track suppression
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioObject_RequestSuppression( NGhostAudioObject *this )
{
	NGhostAudio_Audio_NGhostAudioRemoteAudio_SetIsError( this->m_remote );
}

/**
 * Get audio
 *
 * @param this
 * 		This instance
 *
 * @return the audio instance
 */
NMusique *NGhostAudio_Audio_NGhostAudioObject_GetAudio( const NGhostAudioObject *this )
{
	return this->m_audio;
}

/**
 * Get remote file path
 *
 * @param this
 * 		This instance
 *
 * @return the remote file path
 */
const char *NGhostAudio_Audio_NGhostAudioObject_GetRemoteFilePath( const NGhostAudioObject *this )
{
	return NGhostAudio_Audio_NGhostAudioRemoteAudio_GetFilePath( this->m_remote );
}

/**
 * Get synchronization list
 *
 * @param this
 * 		This instance
 *
 * @return the synchronization list
 */
const NListe *NGhostAudio_Audio_NGhostAudioObject_GetSynchronizationList( const NGhostAudioObject *this )
{
	return this->m_synchronizedList;
}

/**
 * Get authentication entry
 *
 * @param this
 * 		This instance
 *
 * @return the authentication entry
 */
const NAuthenticationEntry *NGhostAudio_Audio_NGhostAudioObject_GetAuthenticationEntry( const NGhostAudioObject *this )
{
	return NGhostAudio_Audio_NGhostAudioRemoteAudio_GetAuthenticationEntry( this->m_remote );
}

/**
 * Clear audio
 *
 * @param this
 * 		This instance
 */
__PRIVATE void NGhostAudio_Audio_NGhostAudioObject_ClearAudio( NGhostAudioObject *this )
{
	// Audio present?
	if( this->m_audio != NULL )
		// Clear
		NLib_Module_FModex_NMusique_Detruire( &this->m_audio );
}

/**
 * Load audio
 *
 * @param this
 * 		This instance
 * @param filePath
 * 		The filepath
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_LoadAudio( NGhostAudioObject *this,
	const char *filePath )
{
	// Clear
	NGhostAudio_Audio_NGhostAudioObject_ClearAudio( this );

	// Load
	if( !( this->m_audio = NLib_Module_FModex_NMusique_Construire( filePath,
		this->m_volume,
		this->m_isLoop ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quit
		return NFALSE;
	}

	// Set initial position
	NLib_Module_FModex_NMusique_DefinirPosition( this->m_audio,
		this->m_initialPosition );

	// OK
	return NTRUE;
}

/**
 * Process operation after load completed
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_ProcessLoadCallback( NGhostAudioObject *this )
{
	// Check for error
	if( NGhostAudio_Audio_NGhostAudioRemoteAudio_IsError( this->m_remote ) )
		return NFALSE;

	// Do we want read to start after file loading?
	if( this->m_isMustStart )
	{
		// Only one player?
		if( NLib_Memoire_NListe_ObtenirNombre( this->m_synchronizedList ) <= 0 )
			return NLib_Module_FModex_NMusique_Lire( this->m_audio );
		// More then one player?
		else
		{
			// Wait for all players to be ready
			while( !NGhostAudio_Audio_NGhostAudioRemoteAudio_IsError( this->m_remote )
				&& !NGhostAudio_Audio_NGhostAudioRemoteAudio_IsSynchronized( this->m_remote ) )
				NLib_Temps_Attendre( 16 );

			// Are we synchronized?
			if( NGhostAudio_Audio_NGhostAudioRemoteAudio_IsSynchronized( this->m_remote ) )
				// Start
				return NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizeStart( this->m_remote,
					0 );
			// We are not synchronized
			else
				// This is a failure
				return NFALSE;
		}
	}

	// Done nothing
	return NTRUE;
}

/**
 * Build state parser output list internal
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostAudio_Audio_NGhostAudioObject_BuildStateParserOutputListInternal( const NGhostAudioObject *this,
	__OUTPUT NParserOutputList *parserOutputList,
	NGhostAudioTrackStateService serviceType,
	const char *root )
{
	// Key
	char key[ 256 ];

	// Build key
	snprintf( key,
		256,
		"%s%s%s",
		root,
		strlen( root ) > 0 ?
			"."
			: "",
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetName( serviceType ) );

	// Add data
	switch( serviceType )
	{
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_READY:
			return NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				NGhostAudio_Audio_NGhostAudioRemoteAudio_IsReady( this->m_remote ) );

		case NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED:
			if( NGhostAudio_Audio_NGhostAudioRemoteAudio_IsReady( this->m_remote ) )
				return NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
					key,
					NLib_Module_FModex_NMusique_EstPause( this->m_audio ) );
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_VOLUME:
			if( NGhostAudio_Audio_NGhostAudioRemoteAudio_IsReady( this->m_remote ) )
				return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
					key,
					NLib_Module_FModex_NMusique_ObtenirVolume( this->m_audio ) );
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_POSITION:
			if( NGhostAudio_Audio_NGhostAudioRemoteAudio_IsReady( this->m_remote ) )
				return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
					key,
					NLib_Module_FModex_NMusique_ObtenirPosition( this->m_audio ) );

		default:
			return NFALSE;
	}
}

/**
 * Build state parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostAudio_Audio_NGhostAudioObject_BuildStateParserOutputList( const NGhostAudioObject *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *root )
{
	// Service type
	NGhostAudioTrackStateService serviceType = (NGhostAudioTrackStateService)0;

	// Iterate
	for( ; serviceType < NGHOST_AUDIO_TRACK_STATE_SERVICES; serviceType++ )
		// Process
		NGhostAudio_Audio_NGhostAudioObject_BuildStateParserOutputListInternal( this,
			parserOutputList,
			serviceType,
			root );

	// OK
	return NTRUE;
}

/**
 * Process REST GET state request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostAudio_Audio_NGhostAudioObject_ProcessRESTGETStateRequest( const NGhostAudioObject *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor )
{
	// Service type
	NGhostAudioTrackStateService serviceType;

	// Process
	switch( serviceType = NGhostAudio_Service_Audio_NGhostAudioTrackStateService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_ROOT:
			return NGhostAudio_Audio_NGhostAudioObject_BuildStateParserOutputList( this,
				parserOutputList,
				"" );

		default:
			return NGhostAudio_Audio_NGhostAudioObject_BuildStateParserOutputListInternal( this,
				parserOutputList,
				serviceType,
				"" );
	}

}

/**
 * Build track parser output list internal
 *
 * @param this
 * 		This instance
 * @param serviceType
 * 		The service type (NGhostAudioTrackService)
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_BuildTrackParserOutputListInternal( const NGhostAudioObject *this,
	NU32 serviceType,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *root )
{
	// Key
	char key[ 256 ];

	// Iterator
	NU32 i;

	// Build key
	snprintf( key,
		256,
		"%s%s%s",
		root,
		strlen( root ) > 0 ?
			"."
			: "",
		NGhostAudio_Service_Audio_NGhostAudioTrackService_GetName( (NGhostAudioTrackService)serviceType ) );

	// Add
	switch( serviceType )
	{
		case NGHOST_AUDIO_TRACK_SERVICE_NAME:
			return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NGhostAudio_Audio_NGhostAudioRemoteAudio_GetTrackName( this->m_remote ) );
		case NGHOST_AUDIO_TRACK_SERVICE_IS_LOOP:
			return NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				NFALSE );
		case NGHOST_AUDIO_TRACK_SERVICE_LENGTH:
			if( NGhostAudio_Audio_NGhostAudioRemoteAudio_IsReady( this->m_remote ) )
				return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
					key,
					NLib_Module_FModex_NMusique_ObtenirDuree( this->m_audio ) );
			break;

		case NGHOST_AUDIO_TRACK_SERVICE_SYNCHRONIZATION_LIST:
			for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_synchronizedList ); i++ )
				NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_synchronizedList,
						i ) );
			break;

		case NGHOST_AUDIO_TRACK_SERVICE_REMOTE:
			return NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildParserOutputList( this->m_remote,
				parserOutputList,
				key );
		case NGHOST_AUDIO_TRACK_SERVICE_STATE:
			return NGhostAudio_Audio_NGhostAudioObject_BuildStateParserOutputList( this,
				parserOutputList,
				key );

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build track parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_BuildTrackParserOutputList( const NGhostAudioObject *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *root )
{
	// Iterator
	NGhostAudioTrackService serviceType = (NGhostAudioTrackService)0;

	// Iterate
	for( ; serviceType < NGHOST_AUDIO_TRACK_SERVICES; serviceType++ )
		NGhostAudio_Audio_NGhostAudioObject_BuildTrackParserOutputListInternal( this,
			serviceType,
			parserOutputList,
			root );

	// OK
	return NTRUE;
}

/**
 * Process track GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_ProcessTrackGETRequest( const NGhostAudioObject *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor )
{
	// Service type
	NGhostAudioTrackService serviceType;

	// Process
	switch( serviceType = NGhostAudio_Service_Audio_NGhostAudioTrackService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_AUDIO_TRACK_SERVICE_ROOT:
			return NGhostAudio_Audio_NGhostAudioObject_BuildTrackParserOutputList( this,
				parserOutputList,
				"" );

		case NGHOST_AUDIO_TRACK_SERVICE_REMOTE:
			return NGhostAudio_Audio_NGhostAudioRemoteAudio_ProcessRESTGETRequest( this->m_remote,
				parserOutputList,
				requestedElement,
				cursor );

		case NGHOST_AUDIO_TRACK_SERVICE_STATE:
			return NGhostAudio_Audio_NGhostAudioObject_ProcessRESTGETStateRequest( this,
				parserOutputList,
				requestedElement,
				cursor );

		default:
			return NGhostAudio_Audio_NGhostAudioObject_BuildTrackParserOutputListInternal( this,
				serviceType,
				parserOutputList,
				"" );
	}
}

/**
 * Modify state property (private)
 *
 * @param this
 * 		This instance
 * @param serviceType
 * 		The service type
 * @param parserOutput
 * 		The parser output associated
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostAudio_Audio_NGhostAudioObject_EditStateProperty( NGhostAudioObject *this,
	NGhostAudioTrackStateService serviceType,
	const NParserOutput *parserOutput )
{
	// Check if ready
	if( !NGhostAudio_Audio_NGhostAudioRemoteAudio_IsReady( this->m_remote ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_CANT_PROCESS_REQUEST );

		// Quit
		return NFALSE;
	}

	// Process
	switch( serviceType )
	{
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED:
			// Check
			if( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_BOOLEAN )
			{
				// Notify
				NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

				// Quit
				return NFALSE;
			}

			// One player
			if( NLib_Memoire_NListe_ObtenirNombre( this->m_synchronizedList ) <= 0 )
			{
				// Perform action
				if( *(NBOOL *)NParser_Output_NParserOutput_GetValue( parserOutput ) == NTRUE )
					return NLib_Module_FModex_NMusique_Pause( this->m_audio );
				else
					return NLib_Module_FModex_NMusique_Lire( this->m_audio );
			}
			// More then one player
			else
			{
				// Perform action
				if( *(NBOOL *)NParser_Output_NParserOutput_GetValue( parserOutput ) == NTRUE )
					return NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizePause( this->m_remote );
				else
					return NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizeStart( this->m_remote,
						NERREUR );
			}

		case NGHOST_AUDIO_TRACK_STATE_SERVICE_VOLUME:
			// Check
			if( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_INTEGER
				|| *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) > 100 )
			{
				// Notify
				NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

				// Quit
				return NFALSE;
			}

			// Save volume
			this->m_volume = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput );

			// Change volume
			if( NLib_Memoire_NListe_ObtenirNombre( this->m_synchronizedList ) <= 0 )
				return NLib_Module_FModex_NMusique_DefinirVolume( this->m_audio,
					this->m_volume );
			else
				return NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizeVolumeSet( this->m_remote,
					this->m_volume );

		case NGHOST_AUDIO_TRACK_STATE_SERVICE_POSITION:
			// Check
			if( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_INTEGER
				|| *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) > NLib_Module_FModex_NMusique_ObtenirDuree( this->m_audio ) )
			{
				// Notify
				NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

				// Quit
				return NFALSE;
			}

			// Set position
			if( NLib_Memoire_NListe_ObtenirNombre( this->m_synchronizedList ) <= 0 )
				return NLib_Module_FModex_NMusique_DefinirPosition( this->m_audio,
					*(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) );
			else
				return NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizePositionSet( this->m_remote,
					*(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) );

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

			// Quit
			return NFALSE;
	}
}

/**
 * Process track state PUT request internal (private)
 *
 * @param this
 * 		This instance
 * @param serviceType
 * 		The service type
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_ProcessTrackStatePUTRequestInternal( NGhostAudioObject *this,
	NGhostAudioTrackStateService serviceType,
	const NParserOutputList *userData )
{
	// Parser output
	const NParserOutput *parserOutput;

	// Process
	switch( serviceType )
	{
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_ROOT:
			break;

		case NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED:
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_VOLUME:
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_POSITION:
			// Get entry
			if( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
				NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetName( serviceType ),
				NFALSE,
				NTRUE ) ) )
			{
				// Notify
				NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

				// Quit
				return NFALSE;
			}

			// Process
			return NGhostAudio_Audio_NGhostAudioObject_EditStateProperty( this,
				serviceType,
				parserOutput );

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Process track state PUT request (private)
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostAudio_Audio_NGhostAudioObject_ProcessTrackStatePUTRequest( NGhostAudioObject *this,
	const char *requestedElement,
	NU32 *cursor,
	const NParserOutputList *userData )
{
	// Service type
	NGhostAudioTrackStateService serviceType;

	// Find service type
	switch( serviceType = NGhostAudio_Service_Audio_NGhostAudioTrackStateService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_ROOT:
			for( serviceType = NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED; serviceType < NGHOST_AUDIO_TRACK_STATE_SERVICES; serviceType++ )
				NGhostAudio_Audio_NGhostAudioObject_ProcessTrackStatePUTRequestInternal( this,
					serviceType,
					userData );
			break;

		case NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED:
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_VOLUME:
		case NGHOST_AUDIO_TRACK_STATE_SERVICE_POSITION:
			return NGhostAudio_Audio_NGhostAudioObject_ProcessTrackStatePUTRequestInternal( this,
				serviceType,
				userData );

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Process track PUT request
 *
 * @param this
 * 		This instance
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in the requested element
 * @param userData
 * 		The user data
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioObject_ProcessTrackPUTRequest( NGhostAudioObject *this,
	const char *requestedElement,
	NU32 *cursor,
	const NParserOutputList *userData )
{
	// Process
	switch( NGhostAudio_Service_Audio_NGhostAudioTrackService_FindService( requestedElement,
		cursor ) )
	{
		case NGHOST_AUDIO_TRACK_SERVICE_STATE:
			return NGhostAudio_Audio_NGhostAudioObject_ProcessTrackStatePUTRequest( this,
				requestedElement,
				cursor,
				userData );

		default:
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
	}
}
