#include "../../include/NGhostAudio.h"

// --------------------------------------------------------
// struct NGhostAudio::Service::Audio::NGhostAudioTrackInfo
// --------------------------------------------------------

/**
 * Destroy track info
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioTrackInfo_Clean( NGhostAudioTrackInfo *this )
{
	// Check
	if( this->m_synchList != NULL )
		// Clean
		NLib_Memoire_NListe_Detruire( &this->m_synchList );
}

/**
 * Extract user track info
 *
 * @param userData
 * 		The user data
 * @param trackInfo
 * 		The extracted track informations
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioPlayer_ExtractUserTrackInfo( const NParserOutputList *userData,
	__OUTPUT NGhostAudioTrackInfo *trackInfo )
{
	// Parser entry
	const NParserOutput *parserOutput;

	// Iterator
	NU32 i;

	// Synch entry
	char *syncEntry;

	// Synch entries
	NParserOutputList *synchEntryList;

	// Check parameter
	if( !userData )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Clear informations
	memset( trackInfo,
		0,
		sizeof( NGhostAudioTrackInfo ) );

	// Init
	trackInfo->m_volume = 100;
	trackInfo->m_isMustStart = NTRUE;
	trackInfo->m_isLoop = NFALSE;
	trackInfo->m_port = NERREUR;

	// Extract track name
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
		NGhostAudio_Service_Audio_NGhostAudioTrackService_GetName( NGHOST_AUDIO_TRACK_SERVICE_NAME ),
		NFALSE,
		NTRUE ) ) != NULL
		&& ( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_STRING
			 || !( trackInfo->m_trackName = NParser_Output_NParserOutput_GetValue( parserOutput ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Extract remote hostname
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGHOSTAUDIO_TRACK_REMOTE_SERVICE_HOSTNAME ),
		NFALSE,
		NTRUE ) ) != NULL
		&& ( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_STRING
			 || !( trackInfo->m_hostname = NParser_Output_NParserOutput_GetValue( parserOutput ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Extract remote port
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGHOSTAUDIO_TRACK_REMOTE_SERVICE_PORT ),
		NFALSE,
		NTRUE ) ) != NULL )
	{
		if( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_INTEGER )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
		}
		else
			trackInfo->m_port = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput );
	}

	// Extract volume
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGHOST_AUDIO_TRACK_STATE_SERVICE_VOLUME ),
		NFALSE,
		NTRUE ) ) != NULL )
	{
		if( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_INTEGER )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
		}
		else
			if( ( trackInfo->m_volume = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) ) >= 100 )
				trackInfo->m_volume = 100;
	}

	// Extract position
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGHOST_AUDIO_TRACK_STATE_SERVICE_POSITION ),
		NFALSE,
		NTRUE ) ) != NULL )
	{
		if( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_INTEGER )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
		}
		else
			trackInfo->m_position = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput );
	}

	// Extract if must start as soon as possible
	if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
		NGhostAudio_Service_Audio_NGhostAudioTrackStateService_GetFullPath( NGHOST_AUDIO_TRACK_STATE_SERVICE_IS_PAUSED ),
		NFALSE,
		NTRUE ) ) != NULL )
	{
		if( NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_BOOLEAN )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
		}
		else
			trackInfo->m_isMustStart = (NBOOL)( !*(NBOOL*)NParser_Output_NParserOutput_GetValue( parserOutput ) );
	}

	// File path
	if( !( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetFullPath( NGHOSTAUDIO_TRACK_REMOTE_SERVICE_FILE_PATH ),
		NFALSE,
		NTRUE ) )
		|| NParser_Output_NParserOutput_GetType( parserOutput ) != NPARSER_OUTPUT_TYPE_STRING
		|| !( trackInfo->m_filePath = NParser_Output_NParserOutput_GetValue( parserOutput ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NFALSE;
	}

	// Build synch list
	if( !( trackInfo->m_synchList = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Memoire_Liberer ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Extract synch list
	if( ( synchEntryList = NParser_Output_NParserOutputList_GetAllOutput( userData,
		NGhostAudio_Service_Audio_NGhostAudioTrackService_GetName( NGHOST_AUDIO_TRACK_SERVICE_SYNCHRONIZATION_LIST ),
		NTRUE,
		NTRUE ) ) != NULL )
	{
		// Iterate
		for( i = 0; i < NParser_Output_NParserOutputList_GetEntryCount( synchEntryList ); i++ )
			// Get entry
			if( ( parserOutput = NParser_Output_NParserOutputList_GetEntry( synchEntryList,
				i ) ) != NULL )
				// Check type
				if( NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_STRING )
					// Duplicate
					if( ( syncEntry = NParser_Output_NParserOutput_GetValueCopy( parserOutput ) ) != NULL )
						// Add to entry list
						NLib_Memoire_NListe_Ajouter( trackInfo->m_synchList,
							syncEntry );

		// Destroy synch entries list
		NParser_Output_NParserOutputList_Destroy( &synchEntryList );
	}

	// OK
	return NTRUE;
}
