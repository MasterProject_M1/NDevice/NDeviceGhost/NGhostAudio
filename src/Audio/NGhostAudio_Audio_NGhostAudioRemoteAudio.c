#include "../../include/NGhostAudio.h"

// -------------------------------------------------
// struct NGhostAudio::Audio::NGhostAudioRemoteAudio
// -------------------------------------------------

/**
 * Download track via SFTP
 *
 * @param this
 * 		This instance
 * @param localDestination
 * 		The local destination
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_DownloadSFTP( NGhostAudioRemoteAudio *this,
	const char *localDestination )
{
	// Authentication detail
	NMethodeAuthentificationSSH *authenticationDetail;

	// Build authentication method
	switch( NDeviceCommon_Authentication_NAuthenticationEntry_GetMethod( this->m_authenticationEntry ) )
	{
		default:
			authenticationDetail = NULL;
			break;

		case NAUTHENTICATION_METHOD_PASSWORD:
			authenticationDetail = NLib_Module_SSH_NMethodeAuthentificationSSH_Construire( NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_authenticationEntry ),
				NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this->m_authenticationEntry ) );
			break;
		case NAUTHENTICATION_METHOD_PRIVATE_KEY:
			authenticationDetail = NLib_Module_SSH_NMethodeAuthentificationSSH_Construire2( NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_authenticationEntry ),
				"",
				NFALSE,
				NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this->m_authenticationEntry ),
				NFALSE );
			break;
	}

	// Check authentication method
	if( !authenticationDetail )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Download
	return NLib_Module_SSH_DownloadFileSFTP( NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( this->m_authenticationEntry ),
		NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( this->m_authenticationEntry ),
		authenticationDetail,
		this->m_remoteFilePath,
		localDestination );
}

/**
 * Audio object download thread
 *
 * @param this
 * 		This instance
 *
 * @return if operation succeeded
 */
__PRIVATE __THREAD NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_DownloadThread( NGhostAudioRemoteAudio *this )
{
	// Local destination
	char localDestination[ 512 ];

	// Result
	NBOOL downloadResult;

	// Create music directory
	NLib_Module_Repertoire_CreerRepertoire( NGHOSTAUDIO_AUDIO_MUSIC_DIRECTORY );

	// Build local destination
	snprintf( localDestination,
		512,
		"%s/%s",
		NGHOSTAUDIO_AUDIO_MUSIC_DIRECTORY,
		this->m_trackName );

	// Check if file exists
	if( !NLib_Fichier_Operation_EstExiste( localDestination ) )
		// Download file
		switch( NDeviceCommon_Authentication_NAuthenticationEntry_GetType( this->m_authenticationEntry ) )
		{
			case NAUTHENTICATION_ENTRY_TYPE_GHOST_TOKEN:
			case NAUTHENTICATION_ENTRY_TYPE_HTTP_BASIC:
				downloadResult = NHTTP_Commun_DownloadFile( NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( this->m_authenticationEntry ),
					NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( this->m_authenticationEntry ),
					this->m_remoteFilePath,
					localDestination,
					NDeviceCommon_Authentication_NAuthenticationEntry_GetUsername( this->m_authenticationEntry ),
					NDeviceCommon_Authentication_NAuthenticationEntry_GetAuthentication( this->m_authenticationEntry ),
					NGHOSTAUDIO_AUDIO_CONNECTION_TIMEOUT );
				break;
			case NAUTHENTICATION_ENTRY_TYPE_SSH_SFTP:
				downloadResult = NGhostAudio_Audio_NGhostAudioRemoteAudio_DownloadSFTP( this,
					localDestination );
				break;

			default:
				// Notify
				NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

				// Error
				this->m_isError = NTRUE;

				// Loading done
				this->m_isLoadingDone = NTRUE;

				// Quit
				return NFALSE;
		}
	else
		// File already here
		downloadResult = NTRUE;

	// Check download result
	if( !downloadResult )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SOCKET_RECV );

		// Error
		this->m_isError = NTRUE;

		// Loading done
		this->m_isLoadingDone = NTRUE;

		// Quit
		return NFALSE;
	}

	// Load file
	if( !NGhostAudio_Audio_NGhostAudioObject_LoadAudio( this->m_ghostAudioObject,
		localDestination ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Error
		this->m_isError = NTRUE;

		// Loading done
		this->m_isLoadingDone = NTRUE;

		// Quit
		return NFALSE;
	}

	// Process start if required
	if( !NGhostAudio_Audio_NGhostAudioObject_ProcessLoadCallback( this->m_ghostAudioObject ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Error
		this->m_isError = NTRUE;

		// Loading done
		this->m_isLoadingDone = NTRUE;

		// Quit
		return NFALSE;
	}

	// OK
	return this->m_isLoadingDone = NTRUE;
}

/**
 * Build remote audio instance
 *
 * @param hostname
 * 		The remote hostname
 * @param port
 * 		The remote port
 * @param authenticationManager
 * 		The authentication manager
 * @param filePath
 * 		The remote file path
 * @param trackName
 * 		The track name
 * @param ghostAudioObject
 * 		The ghost audio object
 *
 * @return the instance
 */
__ALLOC NGhostAudioRemoteAudio *NGhostAudio_Audio_NGhostAudioRemoteAudio_Build( const char *hostname,
	NU32 port,
	const NAuthenticationManager *authenticationManager,
	const char *filePath,
	const char *trackName,
	void *ghostAudioObject )
{
	// Output
	__OUTPUT NGhostAudioRemoteAudio *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostAudioRemoteAudio ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_ghostAudioObject = ghostAudioObject;

	// Lock authentication manager
	NDeviceCommon_Authentication_NAuthenticationManager_ActivateProtection( (NAuthenticationManager*)authenticationManager );

	// Look for authentication entry
	if( !( out->m_authenticationEntry = NDeviceCommon_Authentication_NAuthenticationManager_FindAuthenticationForHostnameAndPort( authenticationManager,
		hostname,
		port ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Unlock authentication manager
		NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( (NAuthenticationManager*)authenticationManager );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Entry is now read only
	NDeviceCommon_Authentication_NAuthenticationEntry_SetReadOnly( (NAuthenticationEntry*)out->m_authenticationEntry );

	// Unlock authentication manager
	NDeviceCommon_Authentication_NAuthenticationManager_DeactivateProtection( (NAuthenticationManager*)authenticationManager );

	// Duplicate data
	if( !( out->m_remoteFilePath = NLib_Chaine_Dupliquer( filePath ) )
		|| !( out->m_trackName = NLib_Chaine_Dupliquer( trackName ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_remoteFilePath );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Remove incorrect characters
	NLib_Chaine_SupprimerCaractere( out->m_trackName,
		'.' );

	// Build download thread
	if( !( out->m_downloadThread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NGhostAudio_Audio_NGhostAudioRemoteAudio_DownloadThread,
		out,
		NULL ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_trackName );
		NFREE( out->m_remoteFilePath );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Build synchronizer
 *
 * @param this
 * 		This instance
 * @param authenticationManager
 * 		The authentication manager
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildSynchronizer( NGhostAudioRemoteAudio *this,
	const NAuthenticationManager *authenticationManager )
{
	// Build synchronizer
	if( !( this->m_synchronizer = NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_Build( NGhostAudio_Audio_NGhostAudioObject_GetSynchronizationList( this->m_ghostAudioObject ),
		this->m_ghostAudioObject,
		authenticationManager ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// There is an error
		this->m_isError = NTRUE;

		// Quit
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Destroy remote audio instance
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioRemoteAudio_Destroy( NGhostAudioRemoteAudio **this )
{
	// Synchronizer active?
	if( (*this)->m_synchronizer != NULL )
		// Destroy synchronizer
		NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_Destroy( &(*this)->m_synchronizer );

	// Wait for download thread to finish
	while( !(*this)->m_isLoadingDone )
		// Wait
		NLib_Temps_Attendre( 16 );

	// Destroy download thread
	NLib_Thread_NThread_Detruire( &(*this)->m_downloadThread );

	// Free
	NFREE( (*this)->m_trackName );
	NFREE( (*this)->m_remoteFilePath );
	NFREE( (*this) );
}

/**
 * Get authentication entry
 *
 * @param this
 * 		This instance
 *
 * @return the authentication entry
 */
const NAuthenticationEntry *NGhostAudio_Audio_NGhostAudioRemoteAudio_GetAuthenticationEntry( const NGhostAudioRemoteAudio *this )
{
	return this->m_authenticationEntry;
}

/**
 * Get remote file path
 *
 * @param this
 * 		This instance
 *
 * @return the remote file path
 */
const char *NGhostAudio_Audio_NGhostAudioRemoteAudio_GetFilePath( const NGhostAudioRemoteAudio *this )
{
	return this->m_remoteFilePath;
}

/**
 * Get track name
 *
 * @param this
 * 		This instance
 *
 * @return the track name
 */
const char *NGhostAudio_Audio_NGhostAudioRemoteAudio_GetTrackName( const NGhostAudioRemoteAudio *this )
{
	return this->m_trackName;
}

/**
 * Is ready?
 *
 * @param this
 * 		This instance
 *
 * @return if ready
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_IsReady( const NGhostAudioRemoteAudio *this )
{
	return (NBOOL)( NGhostAudio_Audio_NGhostAudioObject_GetAudio( this->m_ghostAudioObject ) != NULL
		&& !this->m_isError );
}

/**
 * Is synchronized?
 *
 * @param this
 * 		This instance
 *
 * @return if synchronized with other players
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_IsSynchronized( const NGhostAudioRemoteAudio *this )
{
	return NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_IsSynchronized( this->m_synchronizer );
}

/**
 * Is loading done?
 *
 * @param this
 * 		This instance
 *
 * @return if the loading is done
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_IsLoadingDone( const NGhostAudioRemoteAudio *this )
{
	return this->m_isLoadingDone;
}

/**
 * Is there an error?
 *
 * @param this
 * 		This instance
 *
 * @return if there is an error
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_IsError( const NGhostAudioRemoteAudio *this )
{
	return this->m_isError;
}

/**
 * Activate error flag
 *
 * @param this
 * 		This instance
 */
void NGhostAudio_Audio_NGhostAudioRemoteAudio_SetIsError( NGhostAudioRemoteAudio *this )
{
	this->m_isError = NTRUE;
}

/**
 * Synchronized start
 *
 * @param this
 * 		This instance
 * @param position
 * 		The position at startup (ms, NERREUR to ignore)
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizeStart( NGhostAudioRemoteAudio *this,
	NU32 position )
{
	return NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizeStart( this->m_synchronizer,
		position );
}

/**
 * Synchronized pause
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizePause( NGhostAudioRemoteAudio *this )
{
	return NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizePause( this->m_synchronizer );
}

/**
 * Synchronized volume change
 *
 * @param this
 * 		This instance
 * @param volume
 * 		The new volume to be set
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizeVolumeSet( NGhostAudioRemoteAudio *this,
	NU32 volume )
{
	return NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizeVolumeSet( this->m_synchronizer,
		volume );
}

/**
 * Synchronized position change
 *
 * @param this
 * 		This instance
 * @param position
 * 		The new position to be set
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_SynchronizePositionSet( NGhostAudioRemoteAudio *this,
	NU32 position )
{
	return NGhostAudio_Audio_Synchronization_NGhostAudioSynchronizer_SynchronizePositionSet( this->m_synchronizer,
		position );
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param serviceType
 * 		The service type (NGhostAudioTrackRemoteService)
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildParserOutputListInternal( const NGhostAudioRemoteAudio *this,
	NU32 serviceType,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *root )
{
	// Key
	char key[ 256 ];

	// Build key
	snprintf( key,
		256,
		"%s%s%s",
		root,
		strlen( root ) > 0 ?
			"."
			: "",
		NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_GetName( (NGhostAudioTrackRemoteService)serviceType ) );

	// Add element
	switch( serviceType )
	{
		case NGHOSTAUDIO_TRACK_REMOTE_SERVICE_HOSTNAME:
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				NDeviceCommon_Authentication_NAuthenticationEntry_GetHostname( this->m_authenticationEntry ) );
			break;
		case NGHOSTAUDIO_TRACK_REMOTE_SERVICE_PORT:
			NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				NDeviceCommon_Authentication_NAuthenticationEntry_GetPort( this->m_authenticationEntry ) );
			break;
		case NGHOSTAUDIO_TRACK_REMOTE_SERVICE_FILE_PATH:
			NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
				key,
				this->m_remoteFilePath );
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param root
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildParserOutputList( const NGhostAudioRemoteAudio *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *root )
{
	// Service type
	NGhostAudioTrackRemoteService serviceType = (NGhostAudioTrackRemoteService)0;

	// Iterate
	for( ; serviceType < NGHOSTAUDIO_TRACK_REMOTE_SERVICES; serviceType++ )
		NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildParserOutputListInternal( this,
			serviceType,
			parserOutputList,
			root );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NGhostAudio_Audio_NGhostAudioRemoteAudio_ProcessRESTGETRequest( const NGhostAudioRemoteAudio *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor )
{
	// Service type
	NGhostAudioTrackRemoteService serviceType;

	// Process
	switch( serviceType = NGhostAudio_Service_Audio_NGhostAudioTrackRemoteService_FindService( requestedElement,
		cursor ) )
	{
		default:
			return NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildParserOutputListInternal( this,
				serviceType,
				parserOutputList,
				"" );

		case NGHOSTAUDIO_TRACK_REMOTE_SERVICE_ROOT:
			return NGhostAudio_Audio_NGhostAudioRemoteAudio_BuildParserOutputList( this,
				parserOutputList,
				"" );
	}
}
