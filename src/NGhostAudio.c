#include "../include/NGhostAudio.h"

// ---------------------
// namespace NGhostAudio
// ---------------------

#define GHOST_AUDIO_CONFIGURATION_FILE_PATH		"GhostAudio.conf"

/**
 * Entry point
 *
 * @param argc
 * 		The arguments count
 * @param argv
 * 		The arguments vector
 *
 * @return EXIT_SUCCESS if ok
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Ghost audio
	NGhostAudio *ghostAudio;

	// Init NLib
	NLib_Initialiser( NULL );

	// Build audio
	if( !( ghostAudio = NGhostAudio_NGhostAudio_Build( GHOST_AUDIO_CONFIGURATION_FILE_PATH ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Quit NLib
		NLib_Detruire( );

		// Quit
		return EXIT_FAILURE;
	}

	// Display header
	NDeviceCommon_DisplayHeader( "Audio player",
		NGhostAudio_Configuration_NGhostAudioConfiguration_GetNetworkListeningPort( NGhostAudio_NGhostAudio_GetConfiguration( ghostAudio ) ),
		NGhostAudio_NGhostAudio_GetIP( ghostAudio ) );

	// Wait
	while( NGhostAudio_NGhostAudio_IsRunning( ghostAudio ) )
		// Wait
		NLib_Temps_Attendre( 16 );

	// Destroy ghost audio
	NGhostAudio_NGhostAudio_Destroy( &ghostAudio );

	// Quit NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

